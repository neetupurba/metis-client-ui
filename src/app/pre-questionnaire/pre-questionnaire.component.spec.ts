import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreQuestionnairePageComponent } from './pre-questionnaire-page.component';

describe('PreQuestionnairePageComponent', () => {
  let component: PreQuestionnairePageComponent;
  let fixture: ComponentFixture<PreQuestionnairePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreQuestionnairePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreQuestionnairePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
