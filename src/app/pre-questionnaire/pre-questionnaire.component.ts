import {Component, OnInit} from '@angular/core';
import {StakeholderListChecked} from '../interfaces/stakeholderListChecked';
import {map} from 'rxjs/operators';
import {InteractionService} from '../services/interaction.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment.prod';
import {MetisTexts} from "../constants/commonText";

declare var $: any;

@Component({
  selector: 'app-pre-questionnaire-page',
  templateUrl: './pre-questionnaire.component.html',
  styleUrls: ['./pre-questionnaire.component.css']
})
export class PreQuestionnaireComponent implements OnInit {

  title = MetisTexts.texts.preQuestionnaire
  reviewId: number;
  hideTopStakeholderErrorObj = {};
  hideBottomStakeholderErrorObj = {};
  hideErrorMsg = true;

  APIresult = {};
  questionsData = [];
  headerQuestions = {};
  stakeholdersChecked: StakeholderListChecked[];
  questionStakeholderMapping = {};
  stakeholderQuestionMapping = {};
  stakeholders = [];
  isLoading = true;

  finalJSON = {};

  headerQuestions$;

  textID = "";

  // rm_id from prev. pg using service
  getReviewId(): void {
    this.reviewId = this.interactionService.getReviewId();
  }

  // st.holder list from prev. pg using service
  getStakeholders(): void {
    this.stakeholders = this.interactionService.getStakeholders();
    // this.stakeholders = ["Stakeholder 1", "Stakeholder 2"]
    // console.log(this.stakeholders);
  }

  constructor(private interactionService: InteractionService, private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
    // get API call for questions
    const url = environment.questionall;
    this.http.get(url)
      .subscribe(res => {
        this.APIresult = res;
        this.getReviewId();
        this.getStakeholders();

         //  this.APIresult = {
         //    "ciId":0,
         //    "questionList":[
         //       {
         //          "questionDescription":"Is your site transactional",
         //          "questionType":"binary",
         //          "qid":1,
         //          "pResp":null,
         //          "subQlist":[
         //             {
         //                "questionDescription":"Is this automated",
         //                "questionType":"text",
         //                "qid":2,
         //                "pResp":"true",
         //                "subQlist":null,
         //                "theme":null
         //             },
         //             {
         //                "questionDescription":"Do you have any other consumer transactional touchpoints",
         //                "questionType":"text",
         //                "qid":3,
         //                "pResp":"true",
         //                "subQlist":null,
         //                "theme":null
         //             },
         //             {
         //                "questionDescription":"What are What are the average daily visits to your site",
         //                "questionType":"text",
         //                "qid":4,
         //                "pResp":"false",
         //                "subQlist":null,
         //                "theme":null
         //             },
         //             {
         //                "questionDescription":"If your site is not transactional how do you receive sales data",
         //                "questionType":"text",
         //                "qid":5,
         //                "pResp":"false",
         //                "subQlist":null,
         //                "theme":null
         //             }
         //          ],
         //          "theme":"Pre Review"
         //       },
         //       {
         //          "questionDescription":"Do you use a site analytics tool",
         //          "questionType":"binary",
         //          "qid":6,
         //          "pResp":null,
         //          "subQlist":[
         //             {
         //                "questionDescription":"Which teams have access to site analytics data tool",
         //                "questionType":"text",
         //                "qid":7,
         //                "pResp":"true",
         //                "subQlist":null,
         //                "theme":null
         //             },
         //             {
         //                "questionDescription":"Do you use insights from this tool to inform website design and marketing decisions?- please provide schemas and confirm frequency of access",
         //                "questionType":"text",
         //                "qid":8,
         //                "pResp":"true",
         //                "subQlist":null,
         //                "theme":null
         //             },
         //             {
         //                "questionDescription":"Are the analytics output from this tool utilised in any other decisioning outside the mentioned? - If so, in what capacity?",
         //                "questionType":"text",
         //                "qid":9,
         //                "pResp":"false",
         //                "subQlist":null,
         //                "theme":null
         //             },
         //             {
         //                "questionDescription":"If not, why have you chosen not to? What are the challenges you face?",
         //                "questionType":"text",
         //                "qid":10,
         //                "pResp":"false",
         //                "subQlist":null,
         //                "theme":null
         //             },
         //             {
         //                "questionDescription":"How important is this tool to your daily operations",
         //                "questionType":"text",
         //                "qid":11,
         //                "pResp":null,
         //                "subQlist":null,
         //                "theme":null
         //             }
         //          ],
         //          "theme":"Review"
         //       },
         //       {
         //        "questionDescription":"Is transactional",
         //        "questionType":"binary",
         //        "qid":1000,
         //        "pResp":null,
         //        "subQlist":[
         //           {
         //              "questionDescription":"Is this automated",
         //              "questionType":"text",
         //              "qid":1001,
         //              "pResp":"true",
         //              "subQlist":null,
         //              "theme":null
         //           },
         //           {
         //              "questionDescription":"Do you have any other consumer transactional touchpoints",
         //              "questionType":"text",
         //              "qid":1002,
         //              "pResp":"true",
         //              "subQlist":null,
         //              "theme":null
         //           },
         //           {
         //              "questionDescription":"What are What are the average daily visits to your site",
         //              "questionType":"text",
         //              "qid":1003,
         //              "pResp":"false",
         //              "subQlist":null,
         //              "theme":null
         //           },
         //           {
         //              "questionDescription":"If your site is not transactional how do you receive sales data",
         //              "questionType":"text",
         //              "qid":5,
         //              "pResp":"false",
         //              "subQlist":null,
         //              "theme":null
         //           }
         //        ],
         //        "theme":"Pre Review"
         //     }
         //    ],
         //    "stakeholders":[
         //       "saurabh", "stakeholder2"
         //    ]
         // };

        this.APIresult = {
          "ciId": 0,
          "questionList": [
            {
              "questionDescription": "Is there a long-term plan for investment into data systems e.g. cloud technology or data lakes?",
              "questionType": "binary",
              "qid": 1,
              "theme": "Architecture"
            },
            {
              "questionDescription": "Do you currently utilise any cloud services?",
              "questionType": "binary",
              "qid": 2,
              "theme": "Architecture"
            },
            {
              "questionDescription": "Is your site transactional",
              "questionType": "binary",
              "qid": 3,
              "theme": "Availability"
            },
            {
              "questionDescription": "Do you use a site analytics tool",
              "questionType": "binary",
              "qid": 4,
              "theme": "Availability"
            },
            {
              "questionDescription": "Do you currently have a reporting system in place? ",
              "questionType": "binary",
              "qid": 5,
              "theme": "Application"
            },
            {
              "questionDescription": "Do you have a customer segmentation strategy? ",
              "questionType": "binary",
              "qid": 6,
              "theme": "Application"
            },
            {
              "questionDescription": "Does your business have an analytics team or anyone dedicated to analytics? ",
              "questionType": "binary",
              "qid": 7,
              "theme": "Business Context"
            },
            {
              "questionDescription": "Does your business have a digital team or anyone dedicated to digital?",
              "questionType": "binary",
              "qid": 8,
              "theme": "Business Context"
            },
            {
              "questionDescription": "Do you currently use programmatic channels for digital media execution? ",
              "questionType": "binary",
              "qid": 9,
              "theme": "Activation"
            },
            {
              "questionDescription": "Do you currently use Search for digital media execution? ",
              "questionType": "binary",
              "qid": 10,
              "theme": "Activation"
            }
          ]
        };
        // extracting theme-question mapping
        this.headerQuestions$ = this.http.get(url)
          .pipe(map(res => {
            const obj = {};
            for (let iter = 0; iter < this.questionsData.length; iter++) {
              obj[this.questionsData[iter].theme] = [];
            }
            for (let iter = 0; iter < this.questionsData.length; iter++) {
              obj[this.questionsData[iter].theme].push({
                'qid': this.questionsData[iter].qid,
                'question': this.questionsData[iter].questionDescription
              });
            }
            return obj;
          }));

        this.APIresult["stakeholders"] = this.stakeholders;

        this.questionsData = this.APIresult["questionList"];

        // extracting header-questions from get API response
        for (let iter = 0; iter < this.questionsData.length; iter++) {
          this.headerQuestions[this.questionsData[iter].theme] = [];
        }
        for (let iter = 0; iter < this.questionsData.length; iter++) {
          this.headerQuestions[this.questionsData[iter].theme].push({
            'qid': this.questionsData[iter].qid,
            'question': this.questionsData[iter].questionDescription
          });
        }

        // getting stakeholder data
        this.stakeholdersChecked = [];
        for (let iter = 0; iter < this.APIresult['stakeholders'].length; iter++) {
          let obj: StakeholderListChecked = {
            'stakeholder': this.APIresult['stakeholders'][iter],
            'checked': false
          }
          this.stakeholdersChecked.push(obj);
        }

        this.isLoading = false;


        for (let key in this.headerQuestions) {
          for (let i = 0; i < this.headerQuestions[key].length; i++) {
            this.hideBottomStakeholderErrorObj[this.headerQuestions[key][i].qid] = true;
          }
        }

        // end subscribe here
      }, err => {
        $('#errorHTTPMsgModal').modal('show');
        console.log(err);
      });

  }

  checkQuestionsList() {
    for (let key in this.headerQuestions) {
      for (let i = 0; i < this.headerQuestions[key].length; i++) {
        let question = $("#" + this.headerQuestions[key][i].qid);
        if (question[0].checked) {
          if (!this.checkLength()) {
            this.hideTopStakeholderErrorObj[this.headerQuestions[key][i].qid] = false;
          } else {
            this.hideTopStakeholderErrorObj[this.headerQuestions[key][i].qid] = true;
            this.questionStakeholderMapping[this.headerQuestions[key][i].qid] = [];
            for (let iter = 0; iter < this.stakeholdersChecked.length; iter++) {
              if (this.stakeholdersChecked[iter].checked) {
                this.questionStakeholderMapping[this.headerQuestions[key][i].qid].push(this.stakeholdersChecked[iter].stakeholder);
              }
            }
          }
        }
      }
    }
  }

  // function invoked on check/uncheck of top st.holders
  onCheck(event, stakeholder, checked) {
    if (!event.target.checked) {
      for (let key in this.questionStakeholderMapping) {
        if (this.questionStakeholderMapping[key].indexOf(stakeholder) !== -1) {
          let index = this.questionStakeholderMapping[key].indexOf(stakeholder);
          this.questionStakeholderMapping[key].splice(index, 1);
        }
      }
      if (!this.checkLength()) {
        for (let key in this.headerQuestions) {
          for (let i = 0; i < this.headerQuestions[key].length; i++) {
            this.hideTopStakeholderErrorObj[this.headerQuestions[key][i].qid] = false;
            this.hideBottomStakeholderErrorObj[this.headerQuestions[key][i].qid] = true;
          }
        }
      }

      // let questionLabel = $('.questions');
      // let tempQid, element, flag, eltSibling;
      // for(let iter=0; iter<questionLabel.length; iter++) {
      //   flag = false;
      //   element = questionLabel[iter];
      //   tempQid = element.children[0].id;
      //   eltSibling = element.nextElementSibling.children;
      //   for(let i=0; i<eltSibling.length; i++) {
      //     if(eltSibling[i].tagName === "DIV") {
      //       if(eltSibling[i].children[0].children[0].checked) {
      //         this.hideBottomStakeholderErrorObj[tempQid] = true;
      //         console.log("error hidden", eltSibling[i].children[0].children[0]);
      //         flag = true;
      //       }
      //     }
      //   }
      //   if(flag === false) {
      //     this.hideBottomStakeholderErrorObj[tempQid] = false;
      //     console.log("error unhidden");
      //   }
      // }

    } else {
      for (let key in this.headerQuestions) {
        for (let i = 0; i < this.headerQuestions[key].length; i++) {
          this.hideTopStakeholderErrorObj[this.headerQuestions[key][i].qid] = true;
          this.hideBottomStakeholderErrorObj[this.headerQuestions[key][i].qid] = true;
        }
      }

      for (let key in this.questionStakeholderMapping) {
        this.questionStakeholderMapping[key].push(stakeholder);
      }
      this.checkQuestionsList();

      let checkbox = $('input[id^=checkStakeholder]');
      for (let iter = 0; iter < checkbox.length; iter++) {
        checkbox[iter].checked = true;
      }
    }
  }

  // returns no. of top st.holders checked
  checkLength() {
    let checkedArr = [];
    for (let i = 0; i < this.stakeholdersChecked.length; i++) {
      if (this.stakeholdersChecked[i].checked) {
        checkedArr.push(this.stakeholdersChecked[i].stakeholder);
      }
    }
    if (checkedArr.length === 0) {
      return false;
    } else {
      return true;
    }
  }

  onClickQuestion(qid, event, i, j, k) {
    this.textID = 'collapseQuestions' + i + j;
    let id = document.getElementById('collapseQuestions' + i + j + k);
    if (event.target.checked) {
      if (!this.checkLength()) {
        this.hideTopStakeholderErrorObj[qid] = false;
      } else {
        this.hideTopStakeholderErrorObj[qid] = true;
        this.questionStakeholderMapping[event.target.id] = [];
        for (let iter = 0; iter < this.stakeholdersChecked.length; iter++) {
          if (this.stakeholdersChecked[iter].checked) {
            this.questionStakeholderMapping[event.target.id].push(this.stakeholdersChecked[iter].stakeholder);
          }
        }
      }

      let checkbox = $('input[id^=checkStakeholder]');
      for (let iter = 0; iter < checkbox.length; iter++) {
        checkbox[iter].checked = true;
      }

      $('#collapseQuestions' + i + j).collapse('show');
    } else if (!event.target.checked) {
      this.hideTopStakeholderErrorObj[qid] = true;
      this.hideBottomStakeholderErrorObj[qid] = true;
      if (event.target.id in this.questionStakeholderMapping) {
        delete this.questionStakeholderMapping[event.target.id];
      }
      $('#collapseQuestions' + i + j).collapse('hide');
    }
  }

  // check/uncheck of bottom st.holder
  onClickStakeholder(qid, event, stakeholder) {
    if (!event.target.checked) {
      let index = this.questionStakeholderMapping[qid].indexOf(stakeholder);
      this.questionStakeholderMapping[qid].splice(index, 1);
      if (this.questionStakeholderMapping[qid].length === 0) {
        this.hideBottomStakeholderErrorObj[qid] = false;
      }

    } else if (event.target.checked) {
      if (this.questionStakeholderMapping[qid].indexOf(stakeholder) === -1) {
        this.questionStakeholderMapping[qid].push(stakeholder);
      }
      this.hideBottomStakeholderErrorObj[qid] = true;
    }
  }

  selectAll(theme, iter) {
    for (let i = 0; i < this.headerQuestions[theme].length; i++) {
      $('#collapseQuestions' + iter + i).collapse('show');
      this.questionStakeholderMapping[this.headerQuestions[theme][i].qid] = [];
      for (let j = 0; j < this.stakeholdersChecked.length; j++) {
        if (this.stakeholdersChecked[j].checked) {
          this.questionStakeholderMapping[this.headerQuestions[theme][i].qid].push(this.stakeholdersChecked[j].stakeholder);
        }
        $('#' + this.headerQuestions[theme][i].qid)[0].checked = true;
      }
    }

    let checkbox = $('input[id^=checkStakeholder]');
    for (let iter = 0; iter < checkbox.length; iter++) {
      checkbox[iter].checked = true;
    }
  }

  clearAll(theme, iter) {
    for (let i = 0; i < this.headerQuestions[theme].length; i++) {
      $('#collapseQuestions' + iter + i).collapse('hide');
      if (this.headerQuestions[theme][i].qid in this.questionStakeholderMapping) {
        delete this.questionStakeholderMapping[this.headerQuestions[theme][i].qid];
      }
      $('#' + this.headerQuestions[theme][i].qid)[0].checked = false;
    }

    // let checkbox = $('input[id^=checkStakeholder]');
    // for (let iter = 0; iter < checkbox.length; iter++) {
    //   checkbox[iter].checked = false;
    // }
  }

  onClickSubmit() {
    for (let key in this.questionStakeholderMapping) {
      for (let iter = 0; iter < this.questionStakeholderMapping[key].length; iter++) {
        this.stakeholderQuestionMapping[this.questionStakeholderMapping[key][iter]] = [];
      }
    }
    for (let key in this.questionStakeholderMapping) {
      for (let iter = 0; iter < this.questionStakeholderMapping[key].length; iter++) {
        this.stakeholderQuestionMapping[this.questionStakeholderMapping[key][iter]].push(Number(key));
      }
    }

    if (Object.keys(this.stakeholderQuestionMapping).length === 0) {
      this.hideErrorMsg = false;
    } else {
      this.isLoading = true;
      this.hideErrorMsg = true;
      this.finalJSON["stakeholderQuestionMapping"] = this.stakeholderQuestionMapping;
      this.finalJSON["reviewID"] = this.reviewId;

      // this.finalJSON = {
      //   stakeholderQuestionMapping:{
      //     "Stakeholder 1": [12, 17],
      //     "Stakeholder 2": [12, 17]
      //   },
      //   reviewID: 3325
      // }
      console.log("this.finalJSON:", this.finalJSON);
      const url = environment.review;
      this.http.post(url, this.finalJSON)
        .subscribe(res => {
          this.isLoading = false;
          this.router.navigate(['/reviewquestions']);
        }, err => {
          console.log("client error", err);
          this.isLoading = false;
          $('#errorHTTPMsgModal').modal('show');
        });
    }
  }

  // unused function
  onClickSave() {
    for (let key in this.questionStakeholderMapping) {
      for (let iter = 0; iter < this.questionStakeholderMapping[key].length; iter++) {
        this.stakeholderQuestionMapping[this.questionStakeholderMapping[key][iter]] = [];
      }
    }
    for (let key in this.questionStakeholderMapping) {
      for (let iter = 0; iter < this.questionStakeholderMapping[key].length; iter++) {
        this.stakeholderQuestionMapping[this.questionStakeholderMapping[key][iter]].push(Number(key));
      }
    }
    if (Object.keys(this.stakeholderQuestionMapping).length === 0) {
      this.hideErrorMsg = false;
    } else {
      this.isLoading = true;
      this.hideErrorMsg = true;
      this.finalJSON["stakeholderQuestionMapping"] = this.stakeholderQuestionMapping;
      this.finalJSON["reviewID"] = this.reviewId;

      const url = environment.review;
      this.http.post(url, this.finalJSON)
        .subscribe(res => {
          this.isLoading = false;
          this.router.navigate(['/homepage']);
        }, err => {
          this.isLoading = false;
          // console.log("client error" ,err);
        });
    }

  }

  // unused function
  isAccordianOpen() {
    return $('.question-theme').nextSibling().className.includes('show');
  }

}
