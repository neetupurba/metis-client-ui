import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'groupDataAsDays'
})

export class GroupDataAsDays implements PipeTransform {
    transform(items: any[], daysData: any[]): any[] {
        if(!items) return [];
        if(!daysData) return items;
        return items.filter( it => {
            return this.groupData(it, daysData);
        });
    }

    groupData(item, daysData) {
        if(item.createdDate >= daysData['startDate'] && item.createdDate <= daysData['endDate']) {
            return true;
        }
        else {
            return false;
        }
    }
}