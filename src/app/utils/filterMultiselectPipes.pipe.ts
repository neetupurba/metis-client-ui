import { Pipe, PipeTransform } from '@angular/core';
// import { HomepageDataStatusService } from '../services/homepage-data-status.service';

@Pipe({
 name: 'filtersMultiselect'
})

export class FilterMultiselectPipe implements PipeTransform {
    // constructor(noDataService: HomepageDataStatusService) {

    // }
    // transform(items: any[], clientName: string[], se: string[], status: string[], fromDate: string, toDate: string, noData: boolean): any[] {
    transform(items: any[], clientName: string[], se: string[], status: string[], fromDate: string, toDate: string): any[] {
        if(!items) return [];

        // console.log(clientName);
        items = [...items.filter(it => {
            let result = this.getFilterStatusClientName(it, clientName, "clientName");
            return result;
        })];

        items = [...items.filter(it => {
            let result = this.getFilterStatusClientName(it, se, "se");
            return result;
        })];

        items = [...items.filter(it => {
            let result = this.getFilterStatusClientName(it, status, "status");
            return result;
        })];

        if(fromDate !== "" && toDate !== "") {
            items = [...items.filter(it => {
                let searchText = fromDate + "|" + toDate;
                let result = this.getFilterStatus(it, searchText, "createdDate");
                return result;
            })];
        }
        return items;
    }

    getFilterStatusClientName(item, searchText, searchKey) {
        let flag = false;
        let tempClientName = "";
            for(let key in item) {
                if(typeof item[key] === "string" && key !== "createdDate" && key === searchKey){
                    if(searchText.length === 0) {
                        flag = true;
                    }
                    else {
                        for(let iter=0; iter<searchText.length; iter++) {
                            tempClientName = searchText[iter].toLowerCase();
                            if((item[key].toLowerCase() === tempClientName) || (tempClientName === "")) {
                                flag = true;
                            }
                        }
                    }
                }
                else if(typeof item[key] === "string"  && key === "createdDate"  && key === searchKey) {
                    let fromDate = searchText.split('|')[0];
                    let toDate = searchText.split('|')[1];
                    let date = item[searchKey].split('-');
                    let checkDate = new Date(Number(date[0]), Number(date[1])-1, Number(date[2]));
                    date = fromDate.split('-');
                    let checkFromDate = new Date(Number(date[0]), Number(date[1])-1, Number(date[2]));
                    date = toDate.split('-');
                    let checkToDate = new Date(Number(date[0]), Number(date[1])-1, Number(date[2]));
                    if(checkDate.getTime() >= checkFromDate.getTime() && checkDate.getTime() <= checkToDate.getTime()) {
                        flag = true;
                    }
                }
                else if(typeof item[key] === "object"  && key === searchKey) {
                    for(let iter = 0; iter < item[key].length ; iter++) {
                        if(searchText.length === 0) {
                            flag = true;
                        }
                        else {
                            for(let iter2=0; iter2<searchText.length; iter2++) {
                                tempClientName = searchText[iter2].toLowerCase();
                                if((item[key][iter].toLowerCase() === tempClientName) || (tempClientName === "")) {
                                    flag = true;
                                }
                            }
                        }
                    }
                }
                
                if(flag === true)
                    break;
            }
        if(flag === true)
            return true;
        else
            return false;
    }

    getFilterStatus(item, searchText, searchKey) {
        let flag = false;
        for(let key in item) {
            if(typeof item[key] === "string" && key !== "createdDate" && key === searchKey){
                if((item[key].toLowerCase() === searchText) || (searchText === "")) {
                    flag = true;
                }
            }
            else if(typeof item[key] === "string"  && key === "createdDate"  && key === searchKey) {
                let fromDate = searchText.split('|')[0];
                let toDate = searchText.split('|')[1];
                let date = item[searchKey].split('-');
                let checkDate = new Date(Number(date[0]), Number(date[1])-1, Number(date[2]));
                date = fromDate.split('-');
                let checkFromDate = new Date(Number(date[0]), Number(date[1])-1, Number(date[2]));
                date = toDate.split('-');
                let checkToDate = new Date(Number(date[0]), Number(date[1])-1, Number(date[2]));
                if(checkDate.getTime() >= checkFromDate.getTime() && checkDate.getTime() <= checkToDate.getTime()) {
                    flag = true;
                }
            }
            else if(typeof item[key] === "object"  && key === searchKey) {
                for(let iter = 0; iter < item[key].length ; iter++){
                    if((item[key][iter].toLowerCase() === searchText) || (searchText === "")) {
                        flag = true;
                    }
                }
            }
            
            if(flag === true)
                break;
        }
    if(flag === true)
        return true;
    else
        return false;
    }
}