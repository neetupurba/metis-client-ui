import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'searchFilter'
})
export class SearchPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if(!items) return [];
    if(!searchText) return items;
    // console.log(items);
    searchText = searchText.toLowerCase();
    return items.filter( it => {
        return this.filterTable(it, searchText);
    });
   }

   filterTable(item, searchText){
    //    console.log("in pie:", searchText);
       let flag = false;
        for(let key in item) {            
            if(typeof item[key] === "string" && key !== "createdDate"){
                if(item[key].toLowerCase().includes(searchText)){
                    flag = true;
                }
            }
            else if(typeof item[key] === "string"  && key === "createdDate") {
                const datePipe: DatePipe = new DatePipe('en-US');
                let datestr : string;
                let checkDate = new Date(item[key].split('-')[0], item[key].split('-')[1]-1, item[key].split('-')[2]);
                datestr = datePipe.transform(checkDate, 'longDate');
                if(datestr.toLowerCase().includes(searchText)){
                    flag = true;
                }
            }
            else if(typeof item[key] === "object") {
                for(let iter = 0; iter < item[key].length ; iter++){
                    if(item[key][iter].toLowerCase().includes(searchText)) {
                        flag = true;
                    }
                }
            }
            if(flag === true)
                break;
        }
    if(flag === true)
        return true;
    else
        return false;
   }
}