import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
 name: 'filters'
})

export class FilterPipe implements PipeTransform {
    transform(items: any[], clientName: string, se: string, status: string, fromDate: string, toDate: string): any[] {
        let flag = false;
        if(!items) return [];

        clientName = clientName.toLowerCase();
        items = [...items.filter(it => {
            let result = this.getFilterStatus(it, clientName, "clientName");
            return result;
        })];

        se = se.toLowerCase();
        items = [...items.filter(it => {
            let result = this.getFilterStatus(it, se, "se");
            return result;
        })];

        status = status.toLowerCase();
        items = [...items.filter(it => {
            let result = this.getFilterStatus(it, status, "status");
            return result;
        })];

        if(fromDate !== "" && toDate !== "") {
            items = [...items.filter(it => {
                let searchText = fromDate + "|" + toDate;
                let result = this.getFilterStatus(it, searchText, "createdDate");
                return result;
            })];
        }
        return items;
    }


    getFilterStatus(item, searchText, searchKey){
        let flag = false;
        for(let key in item) {
            if(typeof item[key] === "string" && key !== "createdDate" && key === searchKey){
                if((item[key].toLowerCase() === searchText) || (searchText === "")) {
                    flag = true;
                }
            }
            else if(typeof item[key] === "string"  && key === "createdDate"  && key === searchKey) {
                let fromDate = searchText.split('|')[0];
                let toDate = searchText.split('|')[1];
                let date = item[searchKey].split('-');
                let checkDate = new Date(Number(date[0]), Number(date[1])-1, Number(date[2]));
                date = fromDate.split('-');
                let checkFromDate = new Date(Number(date[0]), Number(date[1])-1, Number(date[2]));
                date = toDate.split('-');
                let checkToDate = new Date(Number(date[0]), Number(date[1])-1, Number(date[2]));
                if(checkDate.getTime() >= checkFromDate.getTime() && checkDate.getTime() <= checkToDate.getTime()) {
                    flag = true;
                }
            }
            else if(typeof item[key] === "object"  && key === searchKey) {
                for(let iter = 0; iter < item[key].length ; iter++){
                    if((item[key][iter].toLowerCase() === searchText) || (searchText === "")) {
                        flag = true;
                    }
                }
            }
            
            if(flag === true)
                break;
        }
    if(flag === true)
        return true;
    else
        return false;
    }
}