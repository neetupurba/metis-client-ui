import { Component, OnInit } from '@angular/core';
import { OktaAuthService } from '../okta-auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location, DatePipe } from '@angular/common';
// import { OktaConstants } from '../../constants/okta-constants';
// import { JwtHelperService } from '@auth0/angular-jwt';

declare var $: any;

@Component({
  selector: 'app-okta-callback',
  templateUrl: './okta-callback.component.html',
  styleUrls: ['./okta-callback.component.css']
})
export class OktaCallbackComponent implements OnInit {

  contactEmail = 'subhan@miqdigital.com';
  unauthorizedUser = false;
  jwtToken;

  constructor(private oktaService: OktaAuthService, private router: Router, private activatedRoute: ActivatedRoute,
              private location: Location, private datePipe: DatePipe) {
    const url = this.router.url;
    // console.log('URL #24:', url);
    const startingIndex = url.indexOf("&access_token=");
    const jwtToken = url.substring(startingIndex + 14, url.indexOf('&token_type'));
    this.jwtToken = jwtToken;
  }

  checkIfUserHasAccess() {
    const url = this.router.url;
    // console.log('url: ', url);
    return !(url.split('error=').length > 1 && url.split('error=')[1].split('&')[0] === 'access_denied');
  }

  ngOnInit() {
    // console.log('jwt:', this.jwtToken);
    // this.location.replaceState(OktaConstants.texts.path);
    // const decodedToken = this.jwtHelper.decodeToken(this.jwtToken);
    // const expiredTime = this.jwtHelper.getTokenExpirationDate(this.jwtToken);
    // const currentTime = new Date();
    // if (expiredTime < currentTime) {
    //   console.log('Expired token');
    //   this.oktaService.loginRedirect();
    //   return;
    // }
    if (this.checkIfUserHasAccess()) {
      this.oktaService.login();
      if (this.oktaService.isAuthenticated()) {
        this.router.navigate(['welcome']);
      } else {
        this.activatedRoute.snapshot.data.onAuthRequired();
      }
    } else {
      this.unauthorizedUser = true;
      // const errorModalId = 'errorModal';
      // $('#' + errorModalId).modal('show');
    }
  }
}
