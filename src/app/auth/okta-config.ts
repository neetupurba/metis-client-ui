export { environment } from '../../environments/environment';
function genRandomString(length: number) {
    const randomCharset = 'abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let random = '';
    for (let c = 0, cl = randomCharset.length; c < length; ++c) {
        random += randomCharset[Math.floor(Math.random() * cl)];
    }
    // console.log('genRandomString()');
    return random;
}

export class OktaConfig {
    static baseUrl = 'https://miq.okta.com';
    // static baseUrl = 'https://miq.oktapreview.com'; /* dev */
    static appBaseUrl =  'http://localhost:4200'; // dev
    // static appBaseUrl = 'https://metis.miqdigital.com'; // prod
    static clientId = '0oa5bag9k8dzGx5PT2p7';
    static redirectUri = OktaConfig.appBaseUrl + '/implicit/callback';
    static issuerUrl = OktaConfig.baseUrl + '/oauth2/default';
    // static googleIdp = '0oa23lbst7BF5ES502p7';
    static state = genRandomString(64);
    static nonce = genRandomString(64);
    static authUrl = OktaConfig.baseUrl + '/oauth2/default/v1/authorize';
    static logoutUrl = OktaConfig.baseUrl + '/oauth2/default/v1/logout';
    static userInfoUrl = OktaConfig.baseUrl + '/oauth2/default/v1/userinfo';
    static postLogoutRedirectUri = OktaConfig.appBaseUrl;
    // static postLogoutRedirectUri = 'https://miqrewards.mediaiqdigital.com';
    static localStorageKeys = {
        accessToken: 'infinity_demo_access_token',
        idToken: 'infinity_demo_id_token'
    };
    static sessionExpiryTimeInHours = 24;
    static sessionsUrl = OktaConfig.baseUrl + '/api/v1/sessions';
}
