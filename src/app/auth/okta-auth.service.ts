import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { OktaConfig } from './okta-config';
import { Observable, of, Observer } from 'rxjs';
import { HttpService } from '../services/http.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OktaAuthService {

  private observers:Observer<boolean>[] = [];
  $authenticationState: Observable<boolean>;
  temporaryCookie = 'Infinity_temp_cookie';
  expiryTime;
  userInfo;

  constructor(private cookieService: CookieService, private router: Router, private http: HttpService) {
    const url = this.router.url;
    console.log("URL:", url);
    this.$authenticationState = new Observable(
      (observer: Observer<boolean>) => {
        this.observers.push(observer);
      }
    );
    const continousSessionCheckInterval = setInterval(() => {
      if (this.cookieService.get(this.temporaryCookie).length <= 0) {
        clearInterval(continousSessionCheckInterval);
        this.emitAuthenticationState(false);
        this.logout();
      }
    }, 10000);
  }

  private parseValueFromUrl(name) {
    const url = this.router.url;
    // console.log('url:', url);
    if(url === undefined) {
      console.log("url not available, url value is undefined");
    }
    const value = url.split(name + '=')[1].split('&')[0];
    if (value !== undefined && value.length > 0) {
      return value;
    }
    return '';
  }

  private getExpiryTimeInMilliSeconds(hours) {
    const timeInMs = hours * 60 * 60 * 1000;
    return new Date(Date.now() + timeInMs);
  }

  emitAuthenticationState(state: boolean) {
    this.observers.forEach(observer => observer.next(state));
  }

  private removeTokensFromLocalStorage() {
    this.cookieService.delete(OktaConfig.localStorageKeys.accessToken);
    this.cookieService.delete(OktaConfig.localStorageKeys.idToken);
  }

  isAuthenticated() {
    const isAuthenticatedUser = !!this.cookieService.get(OktaConfig.localStorageKeys.idToken) && !!this.cookieService.get(OktaConfig.localStorageKeys.accessToken);
    if (!isAuthenticatedUser) {
      this.emitAuthenticationState(false);
    }
    return isAuthenticatedUser;
  }

  getAccessToken() {
    const accessToken = this.cookieService.get(OktaConfig.localStorageKeys.accessToken);
    if (accessToken.length <= 0) {
      this.emitAuthenticationState(false);
    }
    return accessToken;
  }

  getIdToken() {
    const idToken = this.cookieService.get(OktaConfig.localStorageKeys.idToken);
    if (idToken.length <= 0) {
      this.emitAuthenticationState(false);
    }
    return idToken;
  }

  getUser() {
    console.log(this.userInfo);
    if (this.userInfo) {
      return of(this.userInfo);
    }
    return this.http.post(OktaConfig.userInfoUrl, '')
    .pipe(
      tap(info => this.userInfo = info)
    );
  }

  login() {
    console.log('in login')
    const accessToken = this.parseValueFromUrl('access_token');
    const idToken = this.parseValueFromUrl('id_token');
    const expiresIn = this.parseValueFromUrl('expires_in');
    this.expiryTime = expiresIn;
    if (accessToken.length > 0 && idToken.length > 0) {
      // const cookieExpiryTime = new Date(Date.now() + this.expiryTime * 1000);
      const cookieExpiryTime = this.getExpiryTimeInMilliSeconds(OktaConfig.sessionExpiryTimeInHours);
      console.log('expir time ' + cookieExpiryTime);
      this.cookieService.set(this.temporaryCookie, 'valid', cookieExpiryTime);
      this.cookieService.set(OktaConfig.localStorageKeys.accessToken, accessToken);
      this.cookieService.set(OktaConfig.localStorageKeys.idToken, idToken);
      this.emitAuthenticationState(true);
    } else {
      this.emitAuthenticationState(false);
    }
  }

  logout() {
    const idToken = this.cookieService.get(OktaConfig.localStorageKeys.idToken);
    if (idToken.length > 0) {
      const logoutUrl = OktaConfig.logoutUrl + '?id_token_hint=' + idToken + '&post_logout_redirect_uri=' + OktaConfig.postLogoutRedirectUri;
      this.removeTokensFromLocalStorage();
      this.emitAuthenticationState(false);
      window.location.href = logoutUrl;
    }
    return;
  }

  loginRedirect() {
    const params = [];
    params.push('client_id=' + encodeURIComponent(OktaConfig.clientId));
    params.push('response_type=' + encodeURIComponent('id_token token'));
    params.push('scope=' + encodeURIComponent('openid'));
    params.push('redirect_uri=' + encodeURIComponent(OktaConfig.redirectUri));
    params.push('state=' + encodeURIComponent(OktaConfig.state));
    params.push('nonce=' + encodeURIComponent(OktaConfig.nonce));
    const loginUrl = OktaConfig.authUrl + '?' + params.join('&');
    console.log('Redirecting to login page');
    window.location.href = loginUrl;
  }
}
