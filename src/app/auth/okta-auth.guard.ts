import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { OktaAuthService } from './okta-auth.service';

@Injectable({
  providedIn: 'root'
})
export class OktaAuthGuard implements CanActivate{
  constructor(private oktaAuthService: OktaAuthService) {}

  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.oktaAuthService.isAuthenticated()) {
      console.log('Authenticated!!');
      return true;
    }
    // if(next.data !== undefined)
    console.log(next.data);
    next.data.onAuthRequired();
    return false;
  }
}
