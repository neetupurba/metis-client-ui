
export class ClientModal {
    clientName : string; 
    businessType : string;
    businessVertical : string;
    businessSize : string;
    businessGoal : string;
    mediaHoldingGroup : string;
}