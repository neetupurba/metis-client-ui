export const MetisTexts = {
  email: {
    "from": "miqa-gen-support@miqdigital.com",
    "to": "neetu.purba@miqdigital.com,subhan@miqdigital.com,pradhyumna.k@miqdigital.com,saurabhsingh@miqdigital.com",
    "subject": "Metis Pre-questionnaire",
    "attachments": []
  },
  texts: {
    homepageTitle: 'Data Review Hub',
    createReview: 'Client',
    preQuestionnaire: 'Pre-Interview Survey',
    reviewQuestions: 'Survey Review'
  }
};
