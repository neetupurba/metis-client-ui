import { Component, OnInit } from '@angular/core';
import {MetisTexts} from "../constants/commonText";

declare var $: any;

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit {

  stagesNames = MetisTexts.texts;
  currentStep = '';

  constructor() {
  }

  ngOnInit() {
    const currentURL = window.location.href;
    if(currentURL.includes('datareview')){
      this.currentStep = 'datareview';
    } else if (currentURL.includes('prequestionnairepage')){
      this.currentStep = 'prequestionnairepage';
    } else if (currentURL.includes('reviewquestions')){
      this.currentStep = 'reviewquestions';
    }
  }
}
