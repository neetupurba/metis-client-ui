import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-mcq-answer-type',
  templateUrl: './mcq-answer-type.component.html',
  styleUrls: ['./mcq-answer-type.component.css', '../styles.css']
})
export class McqAnswerTypeComponent implements OnInit {

  @Input() options;
  @Output() response = new EventEmitter();
  answerChosen;
  answerOptions;

  constructor() { }

  ngOnInit() {
    this.answerOptions = this.options;
  }

  onAnswerClick(e) {
    this.answerChosen = e.target.innerText;
    if (e.target.classList.contains('answerClickedStyle')) {
      return;
    }
    const buttonsArray = document.querySelectorAll('.answer-btn');
    buttonsArray.forEach(i=>{
      if (i.classList.contains('answerClickedStyle')) {
        i.classList.remove('answerClickedStyle');
      }
    })
    e.target.classList.add('answerClickedStyle');
    this.response.emit(this.answerChosen);
  }

}
