import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { McqAnswerTypeComponent } from './mcq-answer-type.component';

describe('McqAnswerTypeComponent', () => {
  let component: McqAnswerTypeComponent;
  let fixture: ComponentFixture<McqAnswerTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ McqAnswerTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(McqAnswerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
