import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BinaryAnswerTypeComponent } from './binary-answer-type.component';

describe('BinaryAnswerTypeComponent', () => {
  let component: BinaryAnswerTypeComponent;
  let fixture: ComponentFixture<BinaryAnswerTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BinaryAnswerTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BinaryAnswerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
