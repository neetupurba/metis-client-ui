import {Component, EventEmitter, Input, OnInit, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-binary-answer-type',
  templateUrl: './binary-answer-type.component.html',
  styleUrls: ['./binary-answer-type.component.css', '../styles.css']
})
export class BinaryAnswerTypeComponent implements OnInit, OnChanges {

  @Input() options;
  @Output() response = new EventEmitter();
  answerOptions;
  answerChosen;

  constructor() {  }

  ngOnChanges(changes: SimpleChanges) {
    if(!(changes.options.firstChange) && document.querySelectorAll('.answerClickedStyle').length>0){
      document.querySelectorAll('.answerClickedStyle')[0].classList.remove('answerClickedStyle')
    }
  }
  ngOnInit() {
    this.answerOptions = this.options;
  }
  onAnswerClick(e) {
    this.answerChosen = e.target.innerText;
    const buttonsArray = document.querySelectorAll('.binary-btn');
    buttonsArray.forEach(i=>{
      if (i.classList.contains('answerClickedStyle')) {
        i.classList.remove('answerClickedStyle');
      } else if(i.innerHTML === e.target.innerText){
        e.target.classList.add('answerClickedStyle');
      }
    })
    this.response.emit(this.answerChosen);
  }
}
