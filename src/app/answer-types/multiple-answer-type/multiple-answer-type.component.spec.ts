import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleAnswerTypeComponent } from './multiple-answer-type.component';

describe('MultipleAnswerTypeComponent', () => {
  let component: MultipleAnswerTypeComponent;
  let fixture: ComponentFixture<MultipleAnswerTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleAnswerTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleAnswerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
