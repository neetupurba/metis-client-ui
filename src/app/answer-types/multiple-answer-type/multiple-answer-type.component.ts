import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-multiple-answer-type',
  templateUrl: './multiple-answer-type.component.html',
  styleUrls: ['./multiple-answer-type.component.css', '../styles.css']
})
export class MultipleAnswerTypeComponent implements OnInit {

  @Input() options;
  @Output() response = new EventEmitter();
  answerOptions;
  answersChosen = [];
  checkedItems = 0;

  constructor() { }

  ngOnInit() {
    this.answerOptions = this.options;
  }

  onCheckboxClick(e){
    this.answersChosen.push(e.target.innerText);
    if(e.target.checked){
      e.target.parentElement.classList.add('blueBorder');
      this.checkedItems = this.checkedItems + 1;
    }
    else{
      e.target.parentElement.classList.remove('blueBorder');
      this.checkedItems = this.checkedItems - 1;
    }
    this.response.emit(this.answersChosen);
  }
}
