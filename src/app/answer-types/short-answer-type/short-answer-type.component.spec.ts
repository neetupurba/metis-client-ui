import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortAnswerTypeComponent } from './short-answer-type.component';

describe('ShortAnswerTypeComponent', () => {
  let component: ShortAnswerTypeComponent;
  let fixture: ComponentFixture<ShortAnswerTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortAnswerTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortAnswerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
