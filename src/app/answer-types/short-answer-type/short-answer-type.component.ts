import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-short-answer-type',
  templateUrl: './short-answer-type.component.html',
  styleUrls: ['./short-answer-type.component.css', '../styles.css']
})
export class ShortAnswerTypeComponent implements OnInit {

  @Input() options;
  @Output() response = new EventEmitter();
  answerOptions;
  textEntered = "";
  placeholderText = "Type your answer here...";

  constructor() { }

  ngOnInit() {
    this.answerOptions = this.options;
  }

}
