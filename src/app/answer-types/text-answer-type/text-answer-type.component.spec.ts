import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextAnswerTypeComponent } from './text-answer-type.component';

describe('TextAnswerTypeComponent', () => {
  let component: TextAnswerTypeComponent;
  let fixture: ComponentFixture<TextAnswerTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextAnswerTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextAnswerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
