import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-long-answer-type',
  templateUrl: './long-answer-type.component.html',
  styleUrls: ['./long-answer-type.component.css', '../styles.css']
})
export class LongAnswerTypeComponent implements OnInit {

  @Input() options;
  @Output() response = new EventEmitter();
  answerOptions;
  textEntered = "";
  placeholderText = "Type your answer here...";
  constructor() { }

  ngOnInit() {
    this.answerOptions = this.options;
  }

}
