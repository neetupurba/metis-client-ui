import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LongAnswerTypeComponent } from './long-answer-type.component';

describe('LongAnswerTypeComponent', () => {
  let component: LongAnswerTypeComponent;
  let fixture: ComponentFixture<LongAnswerTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LongAnswerTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LongAnswerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
