import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-file-answer-type',
  templateUrl: './file-answer-type.component.html',
  styleUrls: ['./file-answer-type.component.css', '../styles.css']
})
export class FileAnswerTypeComponent implements OnInit {

  @Input() options;
  answerOptions;

  constructor() { }

  ngOnInit() {
    this.answerOptions = this.options;
  }
}
