import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileAnswerTypeComponent } from './file-answer-type.component';

describe('FileAnswerTypeComponent', () => {
  let component: FileAnswerTypeComponent;
  let fixture: ComponentFixture<FileAnswerTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileAnswerTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileAnswerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
