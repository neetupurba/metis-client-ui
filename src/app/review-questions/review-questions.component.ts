import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {InteractionService} from '../services/interaction.service';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment.prod';
import {MetisTexts} from '../constants/commonText';

declare var $: any;

@Component({
  selector: 'app-review-questions',
  templateUrl: './review-questions.component.html',
  styleUrls: ['./review-questions.component.css', './success-modal-animation.css']
})
export class ReviewQuestionsComponent implements OnInit {

  title = MetisTexts.texts.reviewQuestions;
  emailParams = MetisTexts.email;
  sendContentInEmail;
  successModal = false;
  stakeholders: any[];
  stakeholderQuestionList = {};
  stakeholderQuestionList1 = {};
  finalSendEmailJSON = {};
  flag = 'stakeholderQuestions0';
  isLoading = true;
  reviewId: number;
  stakeholderQuestionList$;

  getReviewId(): void {
    this.reviewId = this.interactionService.getReviewId();
  }

  constructor(private http: HttpClient, private interactionService: InteractionService) {
  }

  ngOnInit() {

    // this.stakeholders = ["Client Stakeholder 1", "Client Stakeholder 2", "Client Stakeholder 3", "Client Stakeholder 4", "Client Stakeholder 5"];

    // this.stakeholderQuestionList1 = {
    //   "Ezhil": [
    //     {
    //       "questionDescription": "Is your site transactional",
    //       "qid": 0,
    //       "theme": "Pre Review"
    //     },
    //     {
    //       "questionDescription": "Do you use a site analytics tool",
    //       "qid": 0,
    //       "theme": "Pre Review"
    //     }
    //   ],
    //   "saurabh": [
    //     {
    //       "questionDescription": "Is your site transactional",
    //       "qid": 0,
    //       "theme": "Pre Review"
    //     },
    //     {
    //       "questionDescription": "Do you use a site analytics tool",
    //       "qid": 0,
    //       "theme": "Pre Review"
    //     }
    //   ]
    // };

    this.getReviewId();

    const url = environment.stakeholderQuestionList + "?reviewMasterId=" + this.reviewId;
    this.http.get(url)
      .subscribe(res => {
        this.finalSendEmailJSON = res;
        this.isLoading = false;
        this.stakeholderQuestionList1 = res;

        const obj = {};
        for (let key in this.stakeholderQuestionList1) {
          obj[key] = {};
          for (let i = 0; i < this.stakeholderQuestionList1[key].questionList.length; i++) {
            obj[key][this.stakeholderQuestionList1[key]["questionList"][i].theme] = [];
          }
        }
        for (let key in this.stakeholderQuestionList1) {
          for (let i = 0; i < this.stakeholderQuestionList1[key]["questionList"].length; i++) {
            obj[key][this.stakeholderQuestionList1[key]["questionList"][i].theme].push(this.stakeholderQuestionList1[key]["questionList"][i].questionDescription);
          }
        }
        this.stakeholderQuestionList1 = obj;

        // solution
        this.stakeholderQuestionList$ = this.http.get(url)
          .subscribe(res => {
            const obj = {};
            for (let key in this.stakeholderQuestionList1) {
              obj[key] = {};
              for (let i = 0; i < this.stakeholderQuestionList1[key].length; i++) {
                obj[key][this.stakeholderQuestionList1[key][i].theme] = [];
              }
            }
            for (let key in this.stakeholderQuestionList1) {
              for (let i = 0; i < this.stakeholderQuestionList1[key].length; i++) {
                obj[key][this.stakeholderQuestionList1[key][i].theme].push(this.stakeholderQuestionList1[key][i].questionDescription);
              }
            }
            return obj;
          });

        for (let key in this.stakeholderQuestionList1) {
          this.stakeholderQuestionList[key] = {};
          for (let i = 0; i < this.stakeholderQuestionList1[key].length; i++) {
            this.stakeholderQuestionList[key][this.stakeholderQuestionList1[key][i].theme] = [];
          }
        }

        for (let key in this.stakeholderQuestionList1) {
          for (let i = 0; i < this.stakeholderQuestionList1[key].length; i++) {
            this.stakeholderQuestionList[key][this.stakeholderQuestionList1[key][i].theme].push(this.stakeholderQuestionList1[key][i].questionDescription);
          }
        }

        // this.stakeholderQuestionList = {
        //   "stakeholder1" : {
        //                       "context" : ["Lorem ipsum dolor sit amet, consectetur adipiscing elit?", "Lorem ipsum dolor sit amet?", "Lorem ipsum dolor sit amet, consectetur?"],
        //                       "availability" : ["Lorem ipsum dolor sit amet, consectetur adipiscing elit?", "Lorem ipsum dolor sit amet?"]
        //                   },
        //   "stakeholder2" : {
        //                     "architecture" : ["Lorem ipsum dolor sit amet?", "Lorem ipsum dolor sit amet, consectetur?"],
        //                     "analysis" : ["Lorem ipsum dolor sit amet, consectetur adipiscing elit?"]
        //                   }
        // }

      }, err => {
        console.log(err);
        this.isLoading = false;
        $('#errorHTTPMsgModal').modal('show');
      });
  }

  toggleShow(i, value) {
    this.flag = 'stakeholderQuestions' + i;
  }

  showDetails(value) {
    console.log(typeof value, value[0]);
  }

  sendMail() {
    console.log('finalSendEmailJSON:', this.finalSendEmailJSON, JSON.stringify(this.finalSendEmailJSON));
    const url = environment.sendEmail;
    const email = {
      "from": this.emailParams.from,
      "to": this.emailParams.to,
      "subject": this.emailParams.subject,
      // "body": JSON.stringify(this.finalSendEmailJSON),
      "deliveryStatus": "Deliver",
      "body": "Stakeholder",
      "attachments": this.emailParams.attachments,
    };
    this.isLoading = true;
    this.http.post(url, email)
      .subscribe(res => {
        this.isLoading = false;
        console.log('Res:', res);
        this.successModal = true;
        $(document).ready(function () {
          $('#success_tic').modal('show');
        });
      }, error => {
        console.log('Error: ', error);
        this.isLoading = false;
        $('#errorHTTPMsgModal').modal('show');
      })
  }

}
