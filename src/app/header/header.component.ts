import { Component, OnInit } from '@angular/core';
import {OktaAuthService} from '../auth/okta-auth.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username;

  constructor(private oktaAuth: OktaAuthService) { }

  ngOnInit() {
    this.username = this.oktaAuth.getUser()
      .pipe(map(info => {
        return info['user.fullName'];
      }));
  }
  async logout() {
    await this.oktaAuth.logout();
  }

}
