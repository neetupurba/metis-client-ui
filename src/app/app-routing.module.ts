import { NgModule } from '@angular/core';
import { Routes, RouterModule, UrlSegment } from '@angular/router';
import { DataReviewComponent } from './data-review/data-review.component';
import { PreQuestionnaireComponent } from './pre-questionnaire/pre-questionnaire.component'
import { ReviewQuestionsComponent } from './review-questions/review-questions.component';
import { OktaCallbackComponent } from './auth/okta-callback/okta-callback.component'
import { OktaConfig } from './auth/okta-config';
import { OktaAuthGuard } from './auth/okta-auth.guard';
import { HomepageTrialComponent } from './homepage-trial/homepage-trial.component';
import { HeaderComponent } from './header/header.component';
import {ClientPageComponent} from "./client-page/client-page.component";
import {ClientWelcomePageComponent} from "./client-welcome-page/client-welcome-page.component";

export function onAuthRequired() {
  // Redirect the user to your custom login page
  const params = [];
  params.push('client_id=' + encodeURIComponent(OktaConfig.clientId));
  params.push('response_type=' + encodeURIComponent('id_token token'));
  params.push('scope=' + encodeURIComponent('openid'));
  params.push('redirect_uri=' + encodeURIComponent(OktaConfig.redirectUri));
  params.push('state=' + encodeURIComponent(OktaConfig.state));
  params.push('nonce=' + encodeURIComponent(OktaConfig.nonce));
  const loginUrl = OktaConfig.authUrl + '?' + params.join('&');
  console.log('Login URL:', loginUrl);
  window.location.href = loginUrl;
  // router.navigate(['/login']);
 }

// const appRoutes: Routes = [
//   { path: 'datareview', component: DataReviewComponent },
//   { path: 'homepage', component: HomepageTrialComponent, data: {onAuthRequired}, canActivate: [OktaAuthGuard]},
//   { path: 'prequestionnairepage', component: PreQuestionnaireComponent },
//   { path: 'reviewquestions', component: ReviewQuestionsComponent },
//   // {
//   //   matcher: (url) => {
//   //     console.log(url);
//   //     if (url[0].path.match(/\\?Email=[a-zA-Z0-9]+@gmail.com/)) {
//   //       console.log(url[0]);
//   //       return {
//   //         consumed: url,
//   //         posParams: {
//   //           email: new UrlSegment(url[0].path.substr(1), {})
//   //         }
//   //       };
//   //     }
//   //     return null;
//   //   },
//   //   component: StakeholderQuestionsAnswersComponent
//   // },
//   { path: '',   redirectTo: '/homepage', pathMatch: 'full'},
//   {
//     path: 'implicit/callback',
//     component: OktaCallbackComponent,
//     data: {onAuthRequired}
//   }
// ];

const appRoutes: Routes = [
  {
    path: 'implicit/callback',
    component: OktaCallbackComponent,
    data: {onAuthRequired}
  },
  {
    path: '',
    component: HeaderComponent,
    canActivate: [OktaAuthGuard],
    data: {onAuthRequired},
    children: [
      {
        path: 'homepage', component: HomepageTrialComponent
      },
      {
        path: 'datareview', component: DataReviewComponent
      },
      {
        path: 'prequestionnairepage', component: PreQuestionnaireComponent
      },
      {
        path: 'reviewquestions', component: ReviewQuestionsComponent
      },
      {
        path: 'client', component: ClientPageComponent
      },
      {
        path: 'welcome', component: ClientWelcomePageComponent
      },
      {
        path: '',
        redirectTo: 'welcome', pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'welcome', pathMatch: 'full'
  }
  // {
  //   matcher: (url) => {
  //     console.log(url);
  //     if (url[0].path.match(/\\?Email=[a-zA-Z0-9]+@gmail.com/)) {
  //       console.log(url[0]);
  //       return {
  //         consumed: url,
  //         posParams: {
  //           email: new UrlSegment(url[0].path.substr(1), {})
  //         }
  //       };
  //     }
  //     return null;
  //   },
  //   component: StakeholderQuestionsAnswersComponent
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
