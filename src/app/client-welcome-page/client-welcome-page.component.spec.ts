import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientWelcomePageComponent } from './client-welcome-page.component';

describe('ClientWelcomePageComponent', () => {
  let component: ClientWelcomePageComponent;
  let fixture: ComponentFixture<ClientWelcomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientWelcomePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientWelcomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
