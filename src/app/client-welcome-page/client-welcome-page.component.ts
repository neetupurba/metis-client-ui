import { Component, OnInit } from '@angular/core';
import {ClientService} from "../services/client.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-client-welcome-page',
  templateUrl: './client-welcome-page.component.html',
  styleUrls: ['./client-welcome-page.component.css']
})
export class ClientWelcomePageComponent implements OnInit {

  clientName = "British Gas";
  welcomePara = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio. Lorem ipsum dolor sit amet,"

  constructor(private clientService: ClientService, private router: Router) { }

  ngOnInit() {
  }

  onStartClicked(){
    this.clientService.setStartStatus(true);
    this.clientService.setQuestionNo(0);
    this.router.navigate(["/client"]);
  }

}
