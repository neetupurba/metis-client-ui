import {Injectable} from "@angular/core";

@Injectable()
export class ClientService {
  currentPageNo;
  isStarted = false;
  questionNo;
  questionAnswersJSON;
  maxNoOfQuestion;

  setStartStatus(status) {
    this.isStarted = status
  }

  getStartStatus() {
    return this.isStarted;
  }

  setQuestionNo(no) {
    this.questionNo = no;
  }

  getQuestionNo() {
    return this.questionNo;
  }

  setQAJson(obj) {
    this.questionAnswersJSON = obj.questionList;
    this.maxNoOfQuestion = this.questionAnswersJSON.length;
  }

  getQAJson(qNo) {
    return this.questionAnswersJSON[qNo];
  }

  getMaxNoOfQuestion(){
    return this.maxNoOfQuestion;
  }
}
