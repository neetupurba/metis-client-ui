import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderStatusService {
  private isLoading : boolean = true;
  constructor() { }

  setIsLoading(status : boolean) {
    console.log(this.isLoading);
    this.isLoading = status;
  }

  getIsLoading() : boolean {
    console.log(this.isLoading);
    return this.isLoading;
  }
}
