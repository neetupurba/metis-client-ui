import { Injectable } from '@angular/core';

import { ResponseClientData } from '../interfaces/responseClientData';

@Injectable({
  providedIn: 'root'
})
export class DataPersistanceService {
  private data : ResponseClientData;
  private persistance = false;
  private resultGET = {};

  constructor() { }

  setData(clientData : ResponseClientData) : void {
    this.data = clientData;
    console.log('data set by service', this.data);
  }
  
  getData() : ResponseClientData {
    console.log('data returned by service', this.data);
    return this.data;
  }

  setPersistance(flag : boolean) : void {
    this.persistance = flag;
  }

  getPersistance() : boolean {
    return this.persistance;
  }

  setResultGET(result) {
    this.resultGET = result;
    console.log('data set by resultGET', this.resultGET);
  }

  getResultGET() {
    console.log('data returned by resultGET', this.resultGET);
    return this.resultGET;
  }
}
