import { TestBed } from '@angular/core/testing';

import { DataPersistanceService } from './data-persistance.service';

describe('DataPersistanceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataPersistanceService = TestBed.get(DataPersistanceService);
    expect(service).toBeTruthy();
  });
});
