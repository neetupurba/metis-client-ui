import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OktaAuthService } from '../auth/okta-auth.service';
@Injectable({
 providedIn: 'root'
})
export class HttpService {
 constructor(private http: HttpClient) { }
 get(url: string, options ?: any) {
   return this.http.get(url, options);
 }
 post(url: string, body: string, options?: {}) {
   return this.http.post(url, body, options);
 }
}