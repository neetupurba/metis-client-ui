import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InteractionService {
  private rID : number;
  private stakeholders : string[] =[];

  constructor() { }

  setReviewId(rID : number) : void {
    this.rID = rID;
    console.log(this.rID);
  }

  getReviewId(): number {
    return this.rID;
  }

  setStakeholders(stakeholders : string[]) : void {
    this.stakeholders = stakeholders;
    console.log(this.stakeholders);
  }

  getStakeholders() : string[] {
    return this.stakeholders;
  }
}
