import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomepageDataStatusService {
  private noData = false;
  constructor() { }

  getNoData() : boolean {
    return this.noData;
  }

  setNoData(noData: boolean) {
    this.noData = noData;
  }
}
