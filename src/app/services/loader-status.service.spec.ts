import { TestBed } from '@angular/core/testing';

import { LoaderStatusService } from './loader-status.service';

describe('LoaderStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoaderStatusService = TestBed.get(LoaderStatusService);
    expect(service).toBeTruthy();
  });
});
