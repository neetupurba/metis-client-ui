import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EditService {
  private isEditClicked = false;

  constructor() { }

  getIsEditClicked() : boolean {
    return this.isEditClicked;
  }

  setIsEditClicked(isEditClicked : boolean) {
    this.isEditClicked = isEditClicked;
  }
}
