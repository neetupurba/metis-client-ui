import { TestBed } from '@angular/core/testing';

import { HomepageDataStatusService } from './homepage-data-status.service';

describe('HomepageDataStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HomepageDataStatusService = TestBed.get(HomepageDataStatusService);
    expect(service).toBeTruthy();
  });
});
