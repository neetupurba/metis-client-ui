import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageTrialComponent } from './homepage-trial.component';

describe('HomepageTrialComponent', () => {
  let component: HomepageTrialComponent;
  let fixture: ComponentFixture<HomepageTrialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageTrialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageTrialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
