import {Component, OnInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment.prod';
import {EditService} from '../services/edit.service';
import {LoaderStatusService} from '../services/loader-status.service';
import {MetisTexts} from "../constants/commonText";

declare var $: any;

@Component({
  selector: 'app-homepage-trial',
  templateUrl: './homepage-trial.component.html',
  styleUrls: ['./homepage-trial.component.css']
})

export class HomepageTrialComponent implements OnInit {
  metisTexts = MetisTexts.texts;
  // noData = false;
  dropdownListClient = [];
  selectedItemsClient = [];
  dropdownSettingsClient = {};

  dropdownListSE = [];
  selectedItemsSE = [];
  dropdownSettingsSE = {};

  dropdownListStatus = [];
  selectedItemsStatus = [];
  dropdownSettingsStatus = {};

  isLoading = true;
  isCRUDLoading = false;

  daysData = [];
  days = [];
  tableData1: any = [];
  tableData = [];
  startDate: string;
  endDate: string;
  date: string;
  todayDate = new Date();

  searchText = "";
  attribute: string = 'None';
  clientName = [];
  companyName = [];
  SE = [];
  status = [];
  showAccordion = "";
  showInfoData = false;

  // selectedClientName: string = "";
  selectedClientName : string[] = [];
  selectedCompanyName: string = "";
  // selectedSEName: string = "";
  selectedSEName: string[] = [];
  // selectedStatus: string = "";
  selectedStatus: string[] = [];
  fromDate = "";
  toDate = "";

  displayDateOption: boolean = false;

  // tempSelectedClientName: string = "";
  tempSelectedClientName : string[] = [];
  tempSelectedCompanyName: string = "";
  // tempSelectedSEName: string = "";
  tempSelectedSEName: string[] = [];
  // tempSelectedStatus: string = "";
  tempSelectedStatus: string[] = [];
  tempFromDate = "";
  tempToDate = "";

  constructor(private datePipe: DatePipe, private http: HttpClient, private editService: EditService, private loader: LoaderStatusService) {
  }

  ngOnInit() {
    // $(document).ready(function () {
    //   $('[data-toggle="popover"]').popover()
    // })

    this.days = ["Today", "Yesterday", "Previous 7 days", "Previous 30 days", "Earlier"];
    // logic to group table data according to date
    let obj = {
      day: "",
      startDate: "",
      endDate: ""
    };
    // today date range
    obj.day = this.days[0];
    this.date = this.datePipe.transform(this.todayDate, 'yyyy-MM-dd');
    obj.startDate = this.date;
    obj.endDate = this.date;
    this.daysData.push(obj);

    obj = {
      day: "",
      startDate: "",
      endDate: ""
    };
    // yesterday date range
    obj.day = this.days[1];
    this.todayDate.setDate(this.todayDate.getDate() - 1);
    this.date = this.datePipe.transform(this.todayDate, 'yyyy-MM-dd');
    obj.startDate = this.date;
    obj.endDate = this.date;
    this.daysData.push(obj);

    obj = {
      day: "",
      startDate: "",
      endDate: ""
    };
    // prev 7 days date range
    obj.day = this.days[2];
    this.todayDate = new Date();
    this.todayDate.setDate(this.todayDate.getDate() - 8);
    this.date = this.datePipe.transform(this.todayDate, 'yyyy-MM-dd');
    obj.startDate = this.date;
    this.todayDate = new Date();
    this.todayDate.setDate(this.todayDate.getDate() - 2);
    this.date = this.datePipe.transform(this.todayDate, 'yyyy-MM-dd');
    obj.endDate = this.date;
    this.daysData.push(obj);

    obj = {
      day: "",
      startDate: "",
      endDate: ""
    };
    // prev 30 days date range
    obj.day = this.days[3];
    this.todayDate = new Date();
    this.todayDate.setDate(this.todayDate.getDate() - 39);
    this.date = this.datePipe.transform(this.todayDate, 'yyyy-MM-dd');
    obj.startDate = this.date;
    this.todayDate = new Date();
    this.todayDate.setDate(this.todayDate.getDate() - 9);
    this.date = this.datePipe.transform(this.todayDate, 'yyyy-MM-dd');
    obj.endDate = this.date;
    this.daysData.push(obj);

    obj = {
      day: "",
      startDate: "",
      endDate: ""
    };
    // earlier date range
    obj.day = this.days[4];
    this.todayDate = new Date("01-01-1970")
    this.date = this.datePipe.transform(this.todayDate, 'yyyy-MM-dd');
    obj.startDate = this.date;
    this.todayDate = new Date();
    this.todayDate.setDate(this.todayDate.getDate() - 40);
    this.date = this.datePipe.transform(this.todayDate, 'yyyy-MM-dd');
    obj.endDate = this.date;
    this.daysData.push(obj);

    // console.log(this.daysData);


    // this.tableData.map(i => {
    //   if(this.clientName.indexOf(i.clientName) === -1) {
    //     this.clientName.push(i.clientName);
    //   }

    //   if(this.companyName.indexOf(i.companyName) === -1) {
    //     this.companyName.push(i.companyName);
    //   }
    //   for(var j=0; j<i.se.length; j++) {
    //     if(this.SE.indexOf(i.se[j]) === -1) {
    //       this.SE.push(i.se[j]);
    //     }
    //   }
    //   if(this.status.indexOf(i.status) === -1) {
    //     this.status.push(i.status);
    //   }
    // });

    this.getReviewData();
  }

  // function to call API and populate dropdowns
  getReviewData() {
    // get API
    const url = environment.homePage;
    this.http.get(url)
      .subscribe(res => {
        this.tableData1 = res;
        for (let i = 0; i < this.tableData1.length; i++) {
          if (this.tableData1[i].status === 1) {
            this.tableData1[i].status = "In Progress";
          } else if (this.tableData1[i].status === 2) {
            this.tableData1[i].status = "Completed";
          }
        }
        this.tableData = this.tableData1;
        console.log(this.tableData);

        // get data to populate options in filters
        this.tableData.forEach(i => {
          if (this.clientName.indexOf(i.clientName) === -1) {
            this.clientName.push(i.clientName);
          }
        });

        this.tableData.forEach(i => {
          for (let j = 0; j < i.se.length; j++) {
            if (this.SE.indexOf(i.se[j]) === -1) {
              this.SE.push(i.se[j]);
            }
          }
        });

        this.tableData.forEach(i => {
          if (this.status.indexOf(i.status) === -1) {
            this.status.push(i.status);
          }
        });


        for (let i = 0; i < this.clientName.length; i++) {
          this.dropdownListClient.push({item_id: i + 1, item_text: this.clientName[i]});
          // this.dropdownListClient.push({id: i + 1, itemName: this.clientName[i]});
        }
        this.dropdownSettingsClient = {
          singleSelection: false,
          idField: 'item_id',
          textField: 'item_text',
          itemsShowLimit: 2,
          allowSearchFilter: true
        };

        // this.dropdownSettingsClient = {
        //   singleSelection: false,
        //   text: "Select Client",
        //   selectAllText:'Select All',
        //   unSelectAllText:'UnSelect All',
        //   enableSearchFilter: true,
        // };

        for (let i = 0; i < this.SE.length; i++) {
          this.dropdownListSE.push({item_id: i + 1, item_text: this.SE[i]});
          // this.dropdownListSE.push({id: i + 1, itemName: this.SE[i]});
        }
        this.dropdownSettingsSE = {
          singleSelection: false,
          idField: 'item_id',
          textField: 'item_text',
          itemsShowLimit: 2,
          allowSearchFilter: true
        };
        // this.dropdownSettingsSE = {
        //   singleSelection: false,
        //   text: "Select SE",
        //   selectAllText:'Select All',
        //   unSelectAllText:'UnSelect All',
        //   enableSearchFilter: true,
        // };

        for (let i = 0; i < this.status.length; i++) {
          this.dropdownListStatus.push({item_id: i + 1, item_text: this.status[i]});
          // this.dropdownListStatus.push({id: i + 1, itemName: this.status[i]});
        }
        // $("#moreInfo").popover();

        this.dropdownSettingsStatus = {
          singleSelection: true,
          idField: 'item_id',
          textField: 'item_text',
          itemsShowLimit: 2,
          allowSearchFilter: true
        };
        // this.dropdownSettingsStatus = {
        //   singleSelection: false,
        //   text: "Select Status",
        //   selectAllText:'Select All',
        //   unSelectAllText:'UnSelect All',
        //   enableSearchFilter: true,
        // };

        this.isLoading = false;
      }, err => {
        console.log("err: ", err);
        this.isLoading = false;
        $('#errorHTTPMsgModal').modal('show');
      });
  }

  // showContent(i) {
  //   // $(document).ready(function(){
  //   //   $('[data-toggle="popover"]').popover();
  //   // });
  //   this.showInfoData = true;
  //   console.log($("myPopoverContent" + i));
  //   $("myPopoverContent" + i).style.display = "block";
  //   $('[data-toggle=popover]').popover({
  //     content: $("#myPopoverContent" + i).html(),
  //     html: true
  //   }).click(function() {
  //     $(this).popover('show');
  //   });
  // }

  // select/unselect client name
  // onItemSelectClient(item: any) {
  //   // this.searchText = "";
  //   console.log(item);
  //   if (this.selectedItemsClient.length > 0) {
  //     console.log("inside if");
  //     // this.tempSelectedClientName = this.selectedItemsClient[0].item_text;
  //     this.tempSelectedClientName = [];
  //     for(let i=0; i<this.selectedItemsClient.length; i++) {
  //       this.tempSelectedClientName.push(this.selectedItemsClient[i].item_text);
  //       // this.tempSelectedClientName.push(this.selectedItemsClient[i].itemName);
  //     }
  //   } else {
  //     console.log("insdie else");
  //     // this.tempSelectedClientName = "";
  //     this.tempSelectedClientName = [];
  //   }
  //   console.log(this.selectedItemsClient);
  //   console.log(this.tempSelectedClientName);
  // }
  onItemSelectClient(item: any) {
    // this.searchText = "";
    console.log(item);
    // if(item.length > 0) {
    //   console.log("inside if");
    //   // this.tempSelectedClientName = this.selectedItemsClient[0].item_text;
    //   this.tempSelectedClientName = [];
    //   for(let i=0; i<item.length; i++) {
    //     this.tempSelectedClientName.push(item[i].item_text);
    //     // this.tempSelectedClientName.push(this.selectedItemsClient[i].itemName);
    //   }
    // }
    this.tempSelectedClientName.push(item.item_text);
    console.log(this.tempSelectedClientName);
  }

  onItemSelectAllClient(item: any) {
    console.log(item);
    if(item.length > 0) {
      console.log("inside if");
      // this.tempSelectedClientName = this.selectedItemsClient[0].item_text;
      this.tempSelectedClientName = [];
      for(let i=0; i<item.length; i++) {
        this.tempSelectedClientName.push(item[i].item_text);
        // this.tempSelectedClientName.push(this.selectedItemsClient[i].itemName);
      }
    }
    console.log(this.tempSelectedClientName);
  }

  onItemDeselectClient(item: any) {
    console.log(item);
    // if(item.length > 0) {
      // console.log("inside if");
      let index = this.tempSelectedClientName.indexOf(item.item_text);
      if(index !== -1) {
        this.tempSelectedClientName.splice(index, 1);

      }
    // }
    console.log(this.tempSelectedClientName);
  }

  onItemDeselectAllClient(item: any) {
    console.log(item);
    // console.log("inside if");
    this.tempSelectedClientName = [];
    console.log(this.tempSelectedClientName);
  }

  // select/unselect SE name
  // onItemSelectSE(item: any) {
  //   // this.searchText = "";
  //   console.log(item);
  //   console.log(this.selectedItemsSE);
  //   if (this.selectedItemsSE[0]) {
  //     // this.tempSelectedSEName = this.selectedItemsSE[0].item_text;
  //     this.tempSelectedSEName = [];
  //     for(let i=0; i<this.selectedItemsSE.length; i++) {
  //       this.tempSelectedSEName.push(this.selectedItemsSE[i].item_text);
  //       // this.tempSelectedSEName.push(this.selectedItemsSE[i].itemName);
  //     }
  //   } else {
  //     // this.tempSelectedSEName = "";
  //     this.tempSelectedSEName = [];
  //   }
  // }

  onItemSelectSE(item: any) {
    console.log(item);
    this.tempSelectedSEName.push(item.item_text);
    console.log(this.tempSelectedSEName);
  }

  onItemSelectAllSE(item: any) {
    console.log(item);
    if(item.length > 0) {
      console.log("inside if");
      this.tempSelectedSEName = [];
      for(let i=0; i<item.length; i++) {
        this.tempSelectedSEName.push(item[i].item_text);
      }
    }
    console.log(this.tempSelectedSEName);
  }

  onItemDeselectSE(item: any) {
    console.log(item);
      let index = this.tempSelectedSEName.indexOf(item.item_text);
      if(index !== -1) {
        this.tempSelectedSEName.splice(index, 1);

      }
    console.log(this.tempSelectedSEName);
  }

  onItemDeselectAllSE(item: any) {
    console.log(item);
    this.tempSelectedSEName = [];
    console.log(this.tempSelectedSEName);
  }

  // select/unselect status
  // onItemSelectStatus(item: any) {
  //   // this.searchText = "";
  //   console.log(item);
  //   console.log(this.selectedItemsSE);
  //   if (this.selectedItemsStatus[0]) {
  //     // this.tempSelectedStatus = this.selectedItemsStatus[0].item_text;
  //     this.tempSelectedStatus = [];
  //     for(let i=0; i<this.selectedItemsStatus.length; i++) {
  //       this.tempSelectedStatus.push(this.selectedItemsStatus[i].item_text);
  //       // this.tempSelectedStatus.push(this.selectedItemsStatus[i].itemName);
  //     }
  //   } else {
  //     // this.tempSelectedStatus = "";
  //     this.tempSelectedStatus = [];
  //   }
  // }

  onItemSelectStatus(item: any) {
    console.log(item);
    this.tempSelectedStatus.push(item.item_text);
    console.log(this.tempSelectedStatus);
  }

  onItemSelectAllStatus(item: any) {
    console.log(item);
    if(item.length > 0) {
      console.log("inside if");
      this.tempSelectedStatus = [];
      for(let i=0; i<item.length; i++) {
        this.tempSelectedStatus.push(item[i].item_text);
      }
    }
    console.log(this.tempSelectedStatus);
  }

  onItemDeselectStatus(item: any) {
    console.log(item);
      let index = this.tempSelectedStatus.indexOf(item.item_text);
      if(index !== -1) {
        this.tempSelectedStatus.splice(index, 1);

      }
    console.log(this.tempSelectedStatus);
  }

  onItemDeselectAllStatus(item: any) {
    console.log(item);
    this.tempSelectedStatus = [];
    console.log(this.tempSelectedStatus);
  }


  // search(event) {
  //   // this.isCRUDLoading = true;
  //   var key = event.which || event.keyCode;
  //   if (key === 13) {
  //     this.isCRUDLoading = true;
  //     this.searchText = event.target.value;
  //   }
  //   setTimeout(i => {
  //     this.isCRUDLoading = false;
  //   }, 1500);
  // }

  // search and show loader
  search(event) {
      this.isCRUDLoading = true;
      this.searchText = event.target.value;
      // console.log(this.searchText)
      setTimeout(i => {
      this.isCRUDLoading = false;
    }, 1000);
  }

  // select date filter
  onSelectDate() {
    this.searchText = "";
    this.displayDateOption = !this.displayDateOption;
  }

  onEdit(rm_id) {
    this.isCRUDLoading = true;
    this.editService.setIsEditClicked(true);
    // const url = ;
    // this.http.get(url)
    //   .subscribe(res => {
    //     console.log(res);
    //   }, err => {
    //     console.log(err);
    //   });
  }

  onDelete(rm_id) {
    this.isCRUDLoading = true;
    const url = environment.delete + '?rm_id=' + rm_id;
    this.http.get(url)
      .subscribe(res => {
        console.log(res);
        this.getReviewData();
        this.isCRUDLoading = false;
      }, err => {
        console.log("error with delete operation");
        $('#errorHTTPMsgModal').modal('show');
      });
  }

  // show/hide more info
  showInfo(j, i) {
    if (this.showAccordion !== 'collapse' + j + i) {
      this.showAccordion = 'collapse' + j + i;
    } else {
      this.showAccordion = 'collapse-1';
    }
  }

  clearAllFilters() {
    this.isCRUDLoading = true;
    this.selectedItemsClient = [];
    this.selectedItemsSE = [];
    this.selectedItemsStatus = [];

    // this.tempSelectedClientName = "";
    this.tempSelectedClientName = [];
    // this.tempSelectedSEName = "";
    this.tempSelectedSEName = [];
    // this.tempSelectedStatus = "";
    this.tempSelectedStatus = [];
    this.tempFromDate = "";
    this.tempToDate = "";

    // this.selectedClientName = "";
    this.selectedClientName = [];
    // this.selectedSEName = "";
    this.selectedSEName = [];
    // this.selectedStatus = "";
    this.selectedStatus = [];
    this.fromDate = "";
    this.toDate = "";

    setTimeout(i => {
      this.isCRUDLoading = false;
    }, 1000);
  }

  clearDateFilter() {
    this.fromDate = "";
    this.toDate = "";
    this.tempFromDate = "";
    this.tempToDate = "";
  }

  onUpdateFilters() {
    this.isCRUDLoading = true;
    this.searchText = "";
    this.selectedClientName = this.tempSelectedClientName;
    this.selectedSEName = this.tempSelectedSEName;
    this.selectedStatus = this.tempSelectedStatus;

    this.fromDate = this.tempFromDate;
    this.toDate = this.tempToDate;

    // if((this.tempFromDate !== "" && this.tempToDate !== "") && (this.tempFromDate < this.tempToDate)) {
    //   this.fromDate = this.tempFromDate;
    //   this.toDate = this.tempToDate;
    //   this.hideDateValidityErrorMsg = true;
    //   this.hideDateEmptyErrorMsg = true;
    //   this.closeModal.nativeElement.click();
    // }
    // else if((this.tempFromDate !== "" && this.tempToDate === "") || (this.tempFromDate === "" && this.tempToDate !== "")) {
    //   // alert("Please enter both From and To date");
    //   this.hideDateValidityErrorMsg = true;
    //   this.hideDateEmptyErrorMsg = false;
    // }
    // else if(this.tempFromDate > this.tempToDate) {
    //   // alert("Please enter valid From and To date");
    //   this.hideDateEmptyErrorMsg = true;
    //   this.hideDateValidityErrorMsg = false;
    // }

    setTimeout(i => {
      this.isCRUDLoading = false;
    }, 1000);
  }

  // show loader on sort
  setSortOption(attribute) {
    this.isCRUDLoading = true;
    setTimeout(i => {
      this.isCRUDLoading = false;
    }, 1000);
  }

  // sort
  onSelectSort() {
    let tempTableData = this.tableData.map(x => Object.assign({}, x));
    if (this.attribute == 'None') {

    } else if (this.attribute === 'Owner') {
      tempTableData.sort((a, b) => {
        return a.ownerName.localeCompare(b.ownerName);
      });
    } else if (this.attribute === 'DateCreated') {
      tempTableData.sort((a, b) => {
        let dateA = a.createdDate.split("-");
        let dateB = b.createdDate.split("-");
        return (dateB[0] + dateB[1] + dateB[2]) - (dateA[0] + dateA[1] + dateA[2]);
      });
    }
    return tempTableData;
  }
}
