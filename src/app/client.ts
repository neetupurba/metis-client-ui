import { ClientModal } from './clientModal';
import { ClientStakeholderModal } from './clientStakeholderModal';
import { MediaAgencyModal } from './mediaAgencyModal';

export class Client {
    clientName: string;
    clientModal: ClientModal;
    stakeholders: string;
    clientStakeholderModal : ClientStakeholderModal;
    businessType: string;
    businessVertical: string;
    businessSize: string;
    businessGoal: string;
    mediaAgency: string;
    mediaAgencyModal: MediaAgencyModal;
    mediaHoldingGroup: string;
    marketingGoals1: string;
    marketingGoals2: string;
    marketingGoals3: string;
    marketingBudget: number;
    splitChannelsProgrammatic : boolean;
    SplitChannelsProgrammaticPercent :number;
    splitChannelsSocial : boolean;
    splitChannelsSocialPercent: number;
    splitChannelsSearch : boolean;
    splitChannelsSearchPercent: number;
    splitChannelsTV : boolean;
    splitChannelsTVPercent: number;
    splitChannelsDOOH : boolean;
    splitChannelsDOOHPercent: number;
    infrastructureDevBudget: string;
    keyMarketingChallenges1: string;
    keyMarketingChallenges2: string;
    keyMarketingChallenges3: string;
}