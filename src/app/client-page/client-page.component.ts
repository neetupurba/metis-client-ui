import { Component, OnInit } from '@angular/core';
import {ClientService} from "../services/client.service";

@Component({
  selector: 'app-client-page',
  templateUrl: './client-page.component.html',
  styleUrls: ['./client-page.component.css']
})
export class ClientPageComponent implements OnInit {

  sampleResponse;
  clientName = "British Gas";
  currentQuestionNo;
  currentQAJson;
  backBtn = "Back";
  nextBtn = "Next";
  skipBtn = "Skip";
  maxNoOfQuestion;
  isResponseGiven = false;

  constructor(private clientService: ClientService) {
    this.currentQuestionNo = clientService.getQuestionNo();
  }

  ngOnInit() {
    this.sampleResponse = {
      "questionList": [
        {
          "question": "Does your business have an analytics team or anyone dedicated to analytics? ",
          "questionType": "binary",
          "qId" : 13,
          "parentResponse": null,
          "answerOptions": ["Yes", "No"],
          "isSubQuestion": true,
          "questionList" :
            [
              {
                "parentResponse": "yes",
                "question": "What is the size of this team?",
                "questionType": "multiple choice single select",
                "qId" : null,
                "isSubQuestion": false,
                "answerOptions": ["1-3 persons", "4-7 persons", "8+ persons"]
              }
            ]
        },
        {
          "question": "Does your business have a digital team or anyone dedicated to digital?",
          "questionType": "binary",
          "qId" : 14,
          "parentResponse": null,
          "answerOptions": ["Yes", "No"],
          "isSubQuestion": true,
          "questionList" :
            [
              {
                "parentResponse": "yes",
                "question": "What is the size of this team?",
                "questionType": "multiple choice single select",
                "qId" : null,
                "isSubQuestion": false,
                "answerOptions": ["1-5 persons", "6-10 persons", "11+ persons"]
              }
            ]
        },
        {
          "question": "Does your business have a programmatic advertising team or anyone dedicated to this? ",
          "questionType": "binary",
          "qId" : 15,
          "parentResponse": null,
          "answerOptions": ["Yes", "No"],
          "isSubQuestion": true,
          "questionList" :
            [
              {
                "parentResponse": "yes",
                "question": "What is the size of this team?",
                "questionType": "multiple choice single select",
                "qId" : null,
                "isSubQuestion": false,
                "answerOptions": ["1-3 persons", "4-7 persons", "8+ persons"]
              }
            ]
        },
        {
          "question": "\Which of the following are in-house, outsourced or both?",
            // " • Planning\n" +
            // " • Operational teams (AdOps)\n" +
            // " • Trading\n" +
            // " • Insights\n" +
            // " • Analytics\n" +
            // " • Reporting",
          "questionType": "multi choice multi select",
          "qId" : 69,
          "parentResponse": null,
          "answerOptions": ["Planning", "Operations/AdOps", "Trading", "Insights", "Analytics", "Reporting"],
          "isSubQuestion": false
        },
        {
          "question": "Have you defined your online conversion points?",
          "questionType": "multi choice single select",
          "qId" : 15,
          "parentResponse": null,
          "answerOptions": ["Yes", "No", "Not Applicable"],
          "isSubQuestion": true,
          "questionList" :
            [
              {
                "parentResponse": "yes",
                "question": "How many online conversion points do you have?",
                "questionType": "multiple choice single select",
                "qId" : null,
                "isSubQuestion": false,
                "answerOptions": ["1-5", "6-10 persons", "10+"]
              },
              {
                "parentResponse": "yes",
                "question": "What are conversion points? Please list.",
                "questionType": "short answer text",
                "qId" : null,
                "isSubQuestion": false,
                "answerOptions": null
              },
              {
                "parentResponse": "yes",
                "question": "What is your most frequently hit online conversion point?",
                "questionType": "short answer text",
                "qId" : null,
                "isSubQuestion": false,
                "answerOptions": null
              }
            ]
        },
        {
          "question": "If possible, please upload a map of how your technology and data are connected e.g. a data map. ",
          "questionType": "file upload",
          "qId" : 42,
          "parentResponse": null,
          "answerOptions": ["\n" +
          "File Types: Document type, Spreadsheet, PDF, Presentation, Drawing, Image\n" +
          " \n" +
          " Maximum number of files: 5\n" +
          " \n" +
          " Maximum file size: 10GB"],
          "isSubQuestion": false,
        },
        {
          "question": "Which CMP are you integrated with?",
          "questionType": "short answer text",
          "qId" : 23,
          "parentResponse": null,
          "answerOptions": ["Text"],
          "isSubQuestion": false,
        }
      ]
    };
    this.clientService.setQAJson(this.sampleResponse);
    this.maxNoOfQuestion = this.clientService.getMaxNoOfQuestion();
    this.getQADetails();
  }
  getQADetails() {
    this.currentQAJson = this.clientService.getQAJson(this.currentQuestionNo);
  }
  onBackClick() {
    this.currentQuestionNo = this.currentQuestionNo - 1;
    this.updateDetails();
  }
  onNextClick(){
    this.currentQuestionNo = this.currentQuestionNo + 1;
    this.updateDetails();
  }
  onSkipClick(){
    this.currentQuestionNo = this.currentQuestionNo + 1;
    this.updateDetails();
  }
  updateDetails() {
    this.clientService.setQuestionNo(this.currentQuestionNo);
    this.isResponseGiven = false;
    this.getQADetails();
  }
  getResponse(e){
    this.isResponseGiven = !!e;
  }
}
