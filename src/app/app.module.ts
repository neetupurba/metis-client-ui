import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { DataReviewComponent } from './data-review/data-review.component';
import { DatePipe } from '@angular/common';
import { FilterPipe }from './utils/filterPipes.pipe';
import { SearchPipe }from './utils/search.pipe';
import { ReviewQuestionsComponent } from './review-questions/review-questions.component';
import { KeyExtractPipe } from './utils/keyExtract.pipe';
import { PreQuestionnaireComponent } from './pre-questionnaire/pre-questionnaire.component';
import { InteractionService } from './services/interaction.service';
import {OktaCallbackComponent} from "./auth/okta-callback/okta-callback.component";
import {OktaAuthService} from "./auth/okta-auth.service";
import {OktaAuthGuard} from "./auth/okta-auth.guard";
import {HttpService} from "./services/http.service";
import { CookieService } from 'ngx-cookie-service';
import { HomepageTrialComponent } from './homepage-trial/homepage-trial.component';
import { GroupDataAsDays } from './utils/groupDataAsDays.pipe';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { FilterMultiselectPipe } from './utils/filterMultiselectPipes.pipe';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ClientPageComponent } from './client-page/client-page.component';
import { BinaryAnswerTypeComponent } from './answer-types/binary-answer-type/binary-answer-type.component';
import { McqAnswerTypeComponent } from './answer-types/mcq-answer-type/mcq-answer-type.component';
import { MultipleAnswerTypeComponent } from './answer-types/multiple-answer-type/multiple-answer-type.component';
import { ShortAnswerTypeComponent } from './answer-types/short-answer-type/short-answer-type.component';
import { TextAnswerTypeComponent } from './answer-types/text-answer-type/text-answer-type.component';
import { FileAnswerTypeComponent } from './answer-types/file-answer-type/file-answer-type.component';
import { LongAnswerTypeComponent } from './answer-types/long-answer-type/long-answer-type.component';
import { ClientWelcomePageComponent } from './client-welcome-page/client-welcome-page.component';
import {ClientService} from "./services/client.service";

@NgModule({
  declarations: [
    AppComponent,
    DataReviewComponent,
    FilterPipe,
    SearchPipe,
    FilterMultiselectPipe,
    ReviewQuestionsComponent,
    KeyExtractPipe,
    GroupDataAsDays,
    PreQuestionnaireComponent,
    OktaCallbackComponent,
    HomepageTrialComponent,
    ProgressBarComponent,
    FooterComponent,
    HeaderComponent,
    ClientPageComponent,
    BinaryAnswerTypeComponent,
    McqAnswerTypeComponent,
    MultipleAnswerTypeComponent,
    ShortAnswerTypeComponent,
    TextAnswerTypeComponent,
    FileAnswerTypeComponent,
    LongAnswerTypeComponent,
    ClientWelcomePageComponent,
  ],
  imports: [
    NgMultiSelectDropDownModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    TagInputModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AngularMultiSelectModule
  ],
  providers: [
    DatePipe,
    InteractionService,
    OktaAuthService,
    OktaAuthGuard,
    HttpService,
    CookieService,
    ClientService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
