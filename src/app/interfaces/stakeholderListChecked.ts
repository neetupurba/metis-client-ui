export interface StakeholderListChecked {
    stakeholder : string;
    checked : boolean;
}