export interface SplitChannelPercentage {
    programmatic? : string;
    social? : string;
    search? : string;
    tv? : string;
    dooh? : string;
}