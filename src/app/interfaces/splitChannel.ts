export interface SplitChannel {
    programmatic? : boolean;
    social? : boolean;
    search? : boolean;
    tv? : boolean;
    dooh? : boolean;
}