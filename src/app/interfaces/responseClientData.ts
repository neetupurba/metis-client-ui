export interface ResponseClientData {
    "businessGoal": string;
    "businessSize": string;
    "businessType": string;
    "businessVertical": string;
    "ci_id": number;
    "clientName": string;
    "companyName": string;
    "infraDevlopBudget": string;
    "keyMarketChallenge": string[];
    "marketingBudget": number;
    "marketingGoals": string[];
    "mediaAgency": string;
    "mediaHoldingGroup": string;
    "ownerName": string;
    "seName": string;
    "splitChannelMap": {};
    "stakeholderList": string[];

}