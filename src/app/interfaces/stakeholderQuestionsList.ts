export interface StakeholderQuestionList {
    [key : string] : {
        [key : string] : string[];
    }
}