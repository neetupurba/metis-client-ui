import { Questions } from './questions';

export interface HeaderQuestions {
    [key : string] : Questions[];
}