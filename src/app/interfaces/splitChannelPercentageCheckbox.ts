export interface SplitChannelPercentageCheckbox {
    channel : string;
    percentage? : string;
    checked : boolean;
}