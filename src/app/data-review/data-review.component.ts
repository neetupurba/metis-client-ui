import {Component, OnInit, EventEmitter} from '@angular/core';
import {Client} from '../client';
import {Names} from '../names';
import {ClientStakeholderModal} from '../clientStakeholderModal';
import {MediaAgencyModal} from '../mediaAgencyModal';
import {HttpClient} from '@angular/common/http';
import {ResponseClientData} from '../interfaces/responseClientData';
import {SplitChannelPercentageCheckbox} from '../interfaces/splitChannelPercentageCheckbox';
import {InteractionService} from '../services/interaction.service';
import {DataPersistanceService} from '../services/data-persistance.service';
import {LoaderStatusService} from '../services/loader-status.service';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment.prod';
import {MetisTexts} from "../constants/commonText";

declare var $: any;

@Component({
  selector: 'app-data-review',
  templateUrl: './data-review.component.html',
  styleUrls: ['./data-review.component.css']
})
export class DataReviewComponent implements OnInit {

  title = MetisTexts.texts.createReview;
  hidePercentageErrorMsg = true;
  errorWithMsg = "";
  errorMsg = "";

  dropdownListClient = [];
  selectedItemsClient = [];
  dropdownSettingsClient = {};
  dropdownListStakeholder = [];
  selectedItemsStakeholder = [];
  dropdownSettingsStakeholder = {};

  hideErrorMsg = true;
  hideSplitChannelErrorMsg = true;
  isAddClientModalUsed = false;
  selectedClient: Client;
  flag: boolean = true;
  clientName: Names;
  stakeholders1 = [];
  finalStakeholders = [];
  stakeholders = [];
  businessVerticalList = [];
  businessTypeList = [];
  clientStakeholderModal: ClientStakeholderModal;
  mediaAgencyModal: MediaAgencyModal;
  result = {};
  result2 = {};
  finalClientData: ResponseClientData;
  splitChannelPercentageCheckbox: SplitChannelPercentageCheckbox[];
  clientModalResponse = {};
  businessSizeOptions = ['Small', 'Medium', 'Large'];
  requiredTotalPercentage = 100;

  isLoading = true;
  isCRUDLoading = false;
  addClientError;

  constructor(private http: HttpClient, private interactionService: InteractionService, private dataPersistanceService: DataPersistanceService, private router: Router, private loader: LoaderStatusService) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.clientName = {
      names: null
    };
    this.clientStakeholderModal = {
      name: null,
      email: null,
      jobTitle: null
    };
    this.mediaAgencyModal = {
      name: null
    };
    this.selectedClient = {
      clientName: "",
      clientModal: {
        clientName: "",
        businessType: "",
        businessVertical: "",
        businessSize: "",
        businessGoal: "",
        mediaHoldingGroup: ""
      },
      stakeholders: "",
      clientStakeholderModal: {
        name: "",
        email: "",
        jobTitle: ""
      },
      businessType: "",
      businessVertical: "",
      businessSize: "",
      businessGoal: "",
      mediaAgency: "",
      mediaAgencyModal: {
        name: ""
      },
      mediaHoldingGroup: "",
      marketingGoals1: "",
      marketingGoals2: "",
      marketingGoals3: "",
      marketingBudget: null,
      splitChannelsProgrammatic: null,
      splitChannelsSocial: null,
      splitChannelsSearch: null,
      splitChannelsTV: null,
      splitChannelsDOOH: null,
      SplitChannelsProgrammaticPercent: null,
      splitChannelsSocialPercent: null,
      splitChannelsSearchPercent: null,
      splitChannelsTVPercent: null,
      splitChannelsDOOHPercent: null,
      infrastructureDevBudget: null,
      keyMarketingChallenges1: "",
      keyMarketingChallenges2: "",
      keyMarketingChallenges3: ""
    };
    this.splitChannelPercentageCheckbox = [];
    this.finalClientData = {
      businessGoal: null,
      businessSize: null,
      businessType: null,
      businessVertical: null,
      ci_id: null,
      clientName: null,
      companyName: null,
      infraDevlopBudget: null,
      keyMarketChallenge: [],
      marketingBudget: null,
      marketingGoals: [],
      mediaAgency: null,
      mediaHoldingGroup: null,
      ownerName: null,
      seName: null,
      splitChannelMap: {},
      stakeholderList: []
    };
    this.dropdownSettingsClient = {
      singleSelection: true,
      text: "Select Client",
      enableSearchFilter: true
    };
    this.dropdownSettingsStakeholder = {
      singleSelection: false,
      text: "Select Stakeholders",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "multiselectStakeholder",
      badgeShowLimit: 1,
    };

    // check if browser back btn is used
    if (this.dataPersistanceService.getPersistance()) {
      this.result = this.dataPersistanceService.getResultGET();
      this.result2 = this.result;
      for (let i = 0; i < this.result2['clientStakeholder'].length; i++) {
        if (this.result2['clientStakeholder'][i].stakeholder === null) {
          this.result2['clientStakeholder'][i].stakeholder = [];
        }
      }

      for (let i = 0; i < this.result['clientStakeholder'].length; i++) {
        this.dropdownListClient.push({id: i + 1, itemName: this.result['clientStakeholder'][i].clientName});
      }

      // assigning values to populate in UI
      this.finalClientData = this.dataPersistanceService.getData();
      this.selectedClient.clientName = this.finalClientData.clientName;
      this.selectedItemsClient = [{id: 0, itemName: this.finalClientData.clientName}];
      this.onSelect();
      for (let i = 0; i < this.finalClientData.stakeholderList.length; i++) {
        this.selectedItemsStakeholder.push({id: i + 1, itemName: this.finalClientData.stakeholderList[i]});
        this.stakeholders1.push(this.finalClientData.stakeholderList[i]);
      }
      // this.selectedClient.businessType = this.finalClientData.businessType;

      // extract values to populate business vertical dropdown
      for (let key in this.result['businessVertical']) {
        this.businessVerticalList.push(key);
      }

      this.selectedClient.businessVertical = this.finalClientData.businessVertical;

      this.onChangeBusinessVertical(this.selectedClient.businessVertical);
      this.selectedClient.businessType = this.finalClientData.businessType;

      this.selectedClient.businessSize = this.finalClientData.businessSize;
      this.selectedClient.businessGoal = this.finalClientData.businessGoal;
      this.selectedClient.mediaAgency = this.finalClientData.mediaAgency;
      this.selectedClient.mediaHoldingGroup = this.finalClientData.mediaHoldingGroup;
      if (this.finalClientData.marketingGoals[0] !== "") {
        this.selectedClient.marketingGoals1 = this.finalClientData.marketingGoals[0];
      }
      if (this.finalClientData.marketingGoals[1] !== "") {
        this.selectedClient.marketingGoals2 = this.finalClientData.marketingGoals[1];
      }
      if (this.finalClientData.marketingGoals[2] !== "") {
        this.selectedClient.marketingGoals3 = this.finalClientData.marketingGoals[2];
      }
      // this.selectedClient.marketingGoals1 = this.finalClientData.marketingGoals[0];
      // this.selectedClient.marketingGoals2 = this.finalClientData.marketingGoals[1];
      // this.selectedClient.marketingGoals3 = this.finalClientData.marketingGoals[2];
      this.selectedClient.marketingBudget = this.finalClientData.marketingBudget;
      for (let i = 0; i < this.result['splitChannels'].length; i++) {
        this.splitChannelPercentageCheckbox.push({
          channel: this.result['splitChannels'][i],
          percentage: "",
          checked: false
        });
      }
      for (let i = 0; i < this.splitChannelPercentageCheckbox.length; i++) {
        if (this.splitChannelPercentageCheckbox[i].channel in this.finalClientData.splitChannelMap) {
          this.splitChannelPercentageCheckbox[i].checked = true;
          this.splitChannelPercentageCheckbox[i].percentage = this.finalClientData.splitChannelMap[this.splitChannelPercentageCheckbox[i].channel];
          console.log(this.finalClientData.splitChannelMap[this.splitChannelPercentageCheckbox[i].channel]);
        }
      }
      this.selectedClient.infrastructureDevBudget = this.finalClientData.infraDevlopBudget;
      if (this.finalClientData.keyMarketChallenge[0] !== "") {
        this.selectedClient.keyMarketingChallenges1 = this.finalClientData.keyMarketChallenge[0];
      }
      if (this.finalClientData.keyMarketChallenge[1] !== "") {
        this.selectedClient.keyMarketingChallenges2 = this.finalClientData.keyMarketChallenge[1];
      }
      if (this.finalClientData.keyMarketChallenge[2] !== "") {
        this.selectedClient.keyMarketingChallenges3 = this.finalClientData.keyMarketChallenge[2];
      }
      // this.selectedClient.keyMarketingChallenges1 = this.finalClientData.keyMarketChallenge[0]
      // this.selectedClient.keyMarketingChallenges2 = this.finalClientData.keyMarketChallenge[1];
      // this.selectedClient.keyMarketingChallenges3 = this.finalClientData.keyMarketChallenge[2];

      this.isLoading = false;
    }
    // default routing - call get API
    else {
      const url = environment.createReview;
      this.http.get(url)
        .subscribe(response => {
          this.isLoading = false;
          console.log('success res ', response);
          this.result2 = response;
          this.result = response;
          // make stakeholder array = [] when required
          for (let i = 0; i < this.result2['clientStakeholder'].length; i++) {
            if (this.result2['clientStakeholder'][i].stakeholder === null) {
              this.result2['clientStakeholder'][i].stakeholder = [];
            }
          }
          // this.result['businessSize'] = ['Small', 'Medium', 'Large'];
          this.newCheckbox();

          // populate stakeholder dropdown
          for (let i = 0; i < this.result['clientStakeholder'].length; i++) {
            this.dropdownListClient.push({id: i + 1, itemName: this.result['clientStakeholder'][i].clientName});
          }

          // extract values to populate business vertical dropdown
          for (let key in this.result['businessVertical']) {
            this.businessVerticalList.push(key);
          }
          console.log(this.result);
        }, err => {
          this.isLoading = false;
          console.log("Error:", err);
          $('#errorHTTPMsgModal').modal('show');
        });
    }

    // this.res = {
    //   clientStakeholder: [
    //     {
    //       clientName: "client1",
    //       stakeholder: [
    //         "stakeholder1"
    //       ]
    //     },
    //     {
    //       clientName: "client2",
    //       stakeholder: [
    //         "stakeholder1",
    //         "stakeholder2"
    //       ]
    //     }
    //   ],
    //   businessType: [
    //     "businesstype2",
    //     "businesstype1"
    //   ],
    //   businessVertical: ['Vertical example'],
    //   businessSize: [200,100,300],
    //   mediaAgency: ['Agency example'],
    //   mediaHoldingGroup: ['Group example'],
    //   splitChannels: ["Programmatic","social"],
    //   infraDevBudget: ['Range 1','Range 2']
    //  }

  }

  getBusinessVerticals() {
    return Object.keys(this.result['businessVertical']);
  }

  getBusinessType() {
    return this.result['businessVertical'][this.selectedClient.businessVertical];
  }

  getBusinessTypeWhileClientCreation() {
    return this.result['businessVertical'][this.selectedClient.clientModal.businessVertical];
  }

  // function invoked on select/unselect of client name
  onItemSelectClient(item: any) {
    if (this.selectedItemsClient[0]) {
      this.selectedClient.clientName = this.selectedItemsClient[0].itemName;
      this.onSelect();
    } else {
      this.selectedClient.clientName = "";
    }
  }

  // function to populate business type dropdown
  onChangeBusinessVertical(businessVertical) {
    this.businessTypeList = this.result['businessVertical'][businessVertical];
  }

  createCheckbox(channel: string): { channel: string; percentage?: string; checked: boolean; } {
    let newCheckbox = {channel: "", percentage: null, checked: false};
    newCheckbox.channel = channel;
    return newCheckbox;
  }

  checkIfSplitChannelIsChecked() {
    const arr = this.splitChannelPercentageCheckbox;
    let flag = false;
    let totalPercentage = 0;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].checked === true && arr[i].percentage !== null) {
        totalPercentage += Number(arr[i].percentage);
      }
    }
    if (totalPercentage === this.requiredTotalPercentage)
      flag = true;
    return flag;
  }

  newCheckbox() {
    let newCheckbox;
    for (let i = 0; i < this.result['splitChannels'].length; i++) {
      newCheckbox = this.createCheckbox(this.result['splitChannels'][i]);
      this.splitChannelPercentageCheckbox.push(newCheckbox);
    }
  }

  onSelect() {
    this.flag = false;
    this.stakeholders1 = [];
    this.dropdownListStakeholder = [];
    this.selectedItemsStakeholder = [];

    const selectedClient1 = this.selectedClient.clientName;
    for (let i = 0; i < this.result['clientStakeholder'].length; i++) {
      if (this.result['clientStakeholder'][i].clientName == selectedClient1) {
        for (let j = 0; j < this.result['clientStakeholder'][i].stakeholder.length; j++) {
          this.dropdownListStakeholder.push({
            "id": j + 1,
            "itemName": this.result['clientStakeholder'][i].stakeholder[j]
          });
        }
      }
    }
  }

  onclickAddClientModal() {
    this.isCRUDLoading = true;
    const url = environment.addClient;
    // API for add new client
    this.http.post(url, this.selectedClient.clientModal)
      .subscribe(res => {
        if (res["message"] === "client: " + this.selectedClient.clientModal.clientName + " already exist") {
          this.errorWithMsg = 'Error with ' + res["message"].split(':')[0];
          this.errorMsg = res["message"].split(':')[1];
          $('#errorMsgModal').modal('show');
        } else {
          this.isAddClientModalUsed = true;
          this.clientModalResponse = res;
          this.flag = false;

          this.selectedClient.clientName = this.selectedClient.clientModal.clientName;
          this.selectedClient.businessType = this.selectedClient.clientModal.businessType;
          this.selectedClient.businessVertical = this.selectedClient.clientModal.businessVertical;
          this.selectedClient.businessSize = this.selectedClient.clientModal.businessSize;
          this.selectedClient.businessGoal = this.selectedClient.clientModal.businessGoal;
          this.selectedClient.mediaHoldingGroup = this.selectedClient.clientModal.mediaHoldingGroup;

          // add new client data to overall list, update dropdown
          this.result['clientStakeholder'].push({
            clientName: this.selectedClient.clientName,
            stakeholder: []
          });
          this.stakeholders1 = [];
          this.dropdownListClient.push({id: this.dropdownListClient.length, itemName: this.selectedClient.clientName});
          this.selectedItemsClient = [{id: this.selectedItemsClient, itemName: this.selectedClient.clientName}];
          this.selectedItemsStakeholder = [];
          this.dropdownListStakeholder = [];
        }
        setTimeout(i => {
          this.isCRUDLoading = false;
        }, 500);

        // reset client modal
        this.selectedClient.clientModal.clientName = "";
        this.selectedClient.clientModal.businessType = "";
        this.selectedClient.clientModal.businessVertical = "";
        this.selectedClient.clientModal.businessSize = "";
        this.selectedClient.clientModal.businessGoal = "";
        this.selectedClient.clientModal.mediaHoldingGroup = "";
      }, err => {
        this.isCRUDLoading = false;
        console.log("client error", err);
        // alert("Please fill all the fields before confirm");
        $('#errorHTTPMsgModal').modal('show');
        // reset client modal
        this.selectedClient.clientModal.clientName = "";
        this.selectedClient.clientModal.businessType = "";
        this.selectedClient.clientModal.businessVertical = "";
        this.selectedClient.clientModal.businessSize = "";
        this.selectedClient.clientModal.businessGoal = "";
        this.selectedClient.clientModal.mediaHoldingGroup = "";
      });
  }

  clearClientModal() {
    this.selectedClient.clientModal.clientName = "";
    this.selectedClient.clientModal.businessType = "";
    this.selectedClient.clientModal.businessVertical = "";
    this.selectedClient.clientModal.businessSize = "";
    this.selectedClient.clientModal.businessGoal = "";
    this.selectedClient.clientModal.mediaHoldingGroup = "";
  }

  onclickAddClientStakeholderModal() {
    this.isCRUDLoading = true;
    const url = environment.addStakeholder + "?name=" + encodeURI(this.selectedClient.clientStakeholderModal.name) + "&email=" + encodeURI(this.selectedClient.clientStakeholderModal.email) + "&jobTitle=" + encodeURI(this.selectedClient.clientStakeholderModal.jobTitle);
    // API to add new stakeholder
    this.http.get(url)
      .subscribe(res => {
        if (res["message"] === "Email: " + this.selectedClient.clientStakeholderModal.email + " is already exist") {
          this.errorWithMsg = 'Error with ' + res["message"].split(':')[0];
          this.errorMsg = res["message"].split(':')[1];
          $('#errorMsgModal').modal('show');
        } else {
          let index = this.dropdownListStakeholder.length + 1;
          this.dropdownListStakeholder.push({id: index, itemName: this.selectedClient.clientStakeholderModal.name});
          this.selectedItemsStakeholder.push({id: index, itemName: this.selectedClient.clientStakeholderModal.name})
          for (let i = 0; i < this.result['clientStakeholder'].length; i++) {
            if (this.result['clientStakeholder'][i].clientName === this.selectedClient.clientName) {
              this.result['clientStakeholder'][i].stakeholder.push(this.selectedClient.clientStakeholderModal.name);
              this.stakeholders1.push(this.selectedClient.clientStakeholderModal.name);
            }
          }
          this.selectedClient.stakeholders = this.selectedClient.clientStakeholderModal.name;
        }
        setTimeout(i => {
          this.isCRUDLoading = false;
        }, 500);

        // reset stakeholder modal
        this.selectedClient.clientStakeholderModal.name = "";
        this.selectedClient.clientStakeholderModal.email = "";
        this.selectedClient.clientStakeholderModal.jobTitle = "";
      }, err => {
        this.isCRUDLoading = false;
        console.log("client stakeholder error", err);
        // alert("Please fill all the fields before confirm");
        $('#errorHTTPMsgModal').modal('show');
      });
  }

  clearStakeholderModal() {
    this.selectedClient.clientStakeholderModal.name = "";
    this.selectedClient.clientStakeholderModal.email = "";
    this.selectedClient.clientStakeholderModal.jobTitle = "";
  }

  onclickAddMediaAgencyModal() {
    this.isCRUDLoading = true;
    const url = environment.addMediaAgency + "?name=" + encodeURI(this.selectedClient.mediaAgencyModal.name);
    // API to add media agency
    this.http.get(url)
      .subscribe(res => {
        if (res["message"] === "Media Agency Name: " + this.selectedClient.mediaAgencyModal.name + " is already exist") {
          this.errorWithMsg = 'Error with ' + res["message"].split(':')[0];
          this.errorMsg = res["message"].split(':')[1];
          $('#errorMsgModal').modal('show');
        } else {
          this.result['mediaAgency'].push(this.selectedClient.mediaAgencyModal.name);
          this.selectedClient.mediaAgency = this.selectedClient.mediaAgencyModal.name;
        }
        setTimeout(i => {
          this.isCRUDLoading = false;
        }, 500);
        // reset media agency modal
        this.selectedClient.mediaAgencyModal.name = "";
      }, err => {
        this.isCRUDLoading = false;
        console.log("media agency error", err);
        // alert("Please fill all the fields before confirm");
        $('#errorHTTPMsgModal').modal('show');
      });
  }

  clearMediaAgencyModal() {
    this.selectedClient.mediaAgencyModal.name = "";
  }

  // function invoked on select/unselect of stakeholder
  // onItemSelectStakeholder(item: any) {
  //   this.stakeholders1 = [];
  //   for(let i=0; i<this.selectedItemsStakeholder.length; i++) {
  //     this.stakeholders1.push(this.selectedItemsStakeholder[i].itemName);
  //   }
  //   console.log("onItemSelectStakeholder");
  //   console.log(item);
  //   console.log(this.selectedItemsStakeholder[0]);
  // }

  onItemSelectStakeholder(item: any) {
    // this.stakeholders1 = [];
    this.stakeholders1.push(item.itemName);
  }

  onItemSelectAllStakeholder(item: any) {
    if (item.length > 0) {
      this.stakeholders1 = [];
      for (let i = 0; i < item.length; i++) {
        this.stakeholders1.push(item[i].itemName);
      }
    }
  }

  onItemDeselectStakeholder(item: any) {
    let index = this.stakeholders1.indexOf(item.itemName);
    if (index !== -1) {
      this.stakeholders1.splice(index, 1);
    }
  }

  onItemDeselectAllStakeholder(item: any) {
    this.stakeholders1 = [];
  }

  // onItemDeSelectStakeholder(item: any) {
  //   this.stakeholders1 = [];
  //   for(let i=0; i<this.selectedItemsStakeholder.length; i++) {
  //     this.stakeholders1.push(this.selectedItemsStakeholder[i].itemName);
  //   }
  // }

  onAddStakeholder(event) {
    console.log(event);
    console.log(this.stakeholders1);
  }

  onRemoveStakeholder(event) {
    for (let i = 0; i < this.selectedItemsStakeholder.length; i++) {
      if (event === this.selectedItemsStakeholder[i].itemName) {
        this.selectedItemsStakeholder.splice(i, 1);
      }
    }
  }

  getEmptySelectedClient() {
    return {
      clientName: "",
      clientModal: {
        clientName: "",
        businessType: "",
        businessVertical: "",
        businessSize: "",
        businessGoal: "",
        mediaHoldingGroup: ""
      },
      stakeholders: "",
      clientStakeholderModal: {
        name: "",
        email: "",
        jobTitle: ""
      },
      businessType: "",
      businessVertical: "",
      businessSize: "",
      businessGoal: "",
      mediaAgency: "",
      mediaAgencyModal: {
        name: ""
      },
      mediaHoldingGroup: "",
      marketingGoals1: "",
      marketingGoals2: "",
      marketingGoals3: "",
      marketingBudget: null,
      splitChannelsProgrammatic: null,
      splitChannelsSocial: null,
      splitChannelsSearch: null,
      splitChannelsTV: null,
      splitChannelsDOOH: null,
      SplitChannelsProgrammaticPercent: null,
      splitChannelsSocialPercent: null,
      splitChannelsSearchPercent: null,
      splitChannelsTVPercent: null,
      splitChannelsDOOHPercent: null,
      infrastructureDevBudget: null,
      keyMarketingChallenges1: "",
      keyMarketingChallenges2: "",
      keyMarketingChallenges3: ""
    };
  }

  onClickClearForm() {
    this.selectedItemsClient = [];
    this.selectedItemsStakeholder = [];
    this.stakeholders1 = [];
    this.splitChannelPercentageCheckbox = [];
    this.newCheckbox();
    this.selectedClient = this.getEmptySelectedClient();
    console.log(this.selectedClient);
    this.flag = true;
  }

  // unused function, modify to use
  OnClickSave() {
    if (this.isAddClientModalUsed) {
      this.finalClientData['ci_id'] = this.clientModalResponse['ciId'];
    } else {
      this.finalClientData['ci_id'] = -1;
    }
    if (this.selectedClient.clientName === "" || this.selectedClient.clientName === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.businessGoal === "" || this.selectedClient.businessGoal === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.businessSize === "" || this.selectedClient.businessSize === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.businessType === "" || this.selectedClient.businessType === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.businessVertical === "" || this.selectedClient.businessVertical === null) {
      this.hideErrorMsg = false;
    } else if (this.checkSplitChannelChecked() === 0) {
      this.hideErrorMsg = false;
      this.hideSplitChannelErrorMsg = false;
    } else if (this.checkSplitChannelChecked() > 0) {
      this.hideSplitChannelErrorMsg = true;
    } else if (this.selectedClient.infrastructureDevBudget === "" || this.selectedClient.infrastructureDevBudget === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.keyMarketingChallenges1 === "" || this.selectedClient.keyMarketingChallenges1 === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.keyMarketingChallenges2 === "" || this.selectedClient.keyMarketingChallenges2 === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.keyMarketingChallenges3 === "" || this.selectedClient.keyMarketingChallenges3 === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.marketingGoals1 === "" || this.selectedClient.marketingGoals1 === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.marketingGoals2 === "" || this.selectedClient.marketingGoals2 === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.marketingGoals3 === "" || this.selectedClient.marketingGoals3 === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.marketingBudget === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.mediaAgency === "" || this.selectedClient.mediaAgency === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.mediaHoldingGroup === "" || this.selectedClient.mediaHoldingGroup === null) {
      this.hideErrorMsg = false;
    } else if (this.stakeholders1 === [] || this.stakeholders1 === null) {
      this.hideErrorMsg = false;
    } else if (this.splitChannelPercentageCheckbox.length === 0) {
      this.hideErrorMsg = false;
    } else {
      this.isCRUDLoading = true;
      console.log(this.result);
      // console.log(this.splitChannelPercentageCheckbox);
      // console.log(JSON.stringify(form.value));
      console.log(this.selectedClient);
      console.log(this.finalClientData);
      this.finalClientData.businessGoal = this.selectedClient.businessGoal;
      this.finalClientData['businessSize'] = this.selectedClient.businessSize;
      this.finalClientData['businessType'] = this.selectedClient.businessType;
      this.finalClientData['businessVertical'] = this.selectedClient.businessVertical;
      // this.finalClientData['ci_id'] = this.clientModalResponse['ciId'];


      this.finalClientData['clientName'] = this.selectedClient.clientName;
      this.finalClientData['companyName'] = "Company1";
      this.finalClientData['infraDevlopBudget'] = this.selectedClient.infrastructureDevBudget;
      this.finalClientData['keyMarketChallenge'].push(this.selectedClient.keyMarketingChallenges1);
      this.finalClientData['keyMarketChallenge'].push(this.selectedClient.keyMarketingChallenges2);
      this.finalClientData['keyMarketChallenge'].push(this.selectedClient.keyMarketingChallenges3);
      //  + " | " + this.selectedClient.keyMarketingChallenges2 + " | " + this.selectedClient.keyMarketingChallenges3;
      this.finalClientData['marketingBudget'] = this.selectedClient.marketingBudget;
      this.finalClientData['marketingGoals'].push(this.selectedClient.marketingGoals1);
      this.finalClientData['marketingGoals'].push(this.selectedClient.marketingGoals2);
      this.finalClientData['marketingGoals'].push(this.selectedClient.marketingGoals3);
      //  + " | " + this.selectedClient.marketingGoals2 + " | " + this.selectedClient.marketingGoals3;
      this.finalClientData['mediaAgency'] = this.selectedClient.mediaAgency;
      this.finalClientData['mediaHoldingGroup'] = this.selectedClient.mediaHoldingGroup;
      this.finalClientData['ownerName'] = "Albert";
      this.finalClientData['seName'] = "Saurabh";
      this.finalClientData['stakeholderList'] = this.stakeholders1;
      this.interactionService.setStakeholders(this.stakeholders1);
      for (let j = 0; j < this.splitChannelPercentageCheckbox.length; j++) {
        if (this.splitChannelPercentageCheckbox[j].checked) {
          let obj = {
            [this.splitChannelPercentageCheckbox[j].channel]: this.splitChannelPercentageCheckbox[j].percentage
          }
          this.finalClientData['splitChannelMap'] = obj;
          // let obj = {};
          // obj[this.splitChannelPercentageCheckbox[j].channel] = this.splitChannelPercentageCheckbox[j].channel;
          // obj[this.splitChannelPercentageCheckbox[j].percentage] = this.splitChannelPercentageCheckbox[j].percentage;
          // obj[this.splitChannelPercentageCheckbox[j].checked] = this.splitChannelPercentageCheckbox[j].che;
          // this.finalClientData['splitChannelMap'] = obj;
        }
      }
      // this.finalClientData['splitChannelMap']

      this.finalClientData = {
        infraDevlopBudget: "0-100",
        businessType: "Science",
        businessVertical: "Science",
        businessSize: "Medium",
        businessGoal: "Goals",
        ci_id: -1,
        clientName: "Client 1",
        companyName: "Company1",
        keyMarketChallenge: ["Challenge"],
        marketingBudget: 7890,
        marketingGoals: ["hihi"],
        mediaAgency: "Media agency 104",
        mediaHoldingGroup: "Radio",
        ownerName: "Tom Richard",
        seName: "Saurabh",
        splitChannelMap: {"TV": "100"},
        stakeholderList: ["Stakeholder 1"]
      }

      const url = environment.addReview;
      this.http.post(url, this.finalClientData)
        .subscribe(res => {
          this.isCRUDLoading = false;
          this.interactionService.setReviewId(res['reviewMaster']['rm_id']);
          // window.location.href = '/homepage';
          this.router.navigate(['/prequestionnairepage']);

          // this.interactionService.setStakeholders(this.stakeholders1);
        }, err => {
          this.isCRUDLoading = false;
          console.log("client error", err);
          // alert("Please fill all the fields before confirm");
          $('#errorHTTPMsgModal').modal('show');
        });
    }
  }

  // function invoked on check of split channels
  onCheckboxChange(event) {
    // console.log(event);
    console.log(this.splitChannelPercentageCheckbox);
  }

  onClickPercentage(event, i) {
    this.splitChannelPercentageCheckbox[i].percentage = event.target.value;
    console.log(this.splitChannelPercentageCheckbox, this.splitChannelPercentageCheckbox);
  }

  checkSplitChannelPercentageTotal() {
    let totalPercentage = 0;
    let isChecked = false;
    let splitChannelObj = Object.values(this.splitChannelPercentageCheckbox);
    for (let i = 0; i < splitChannelObj.length; i++) {
      console.log(splitChannelObj[i].percentage);
      if (splitChannelObj[i].percentage) {
        isChecked = true;
        break;
      }
    }
    if (isChecked === true) {
      splitChannelObj.forEach(i => {
        totalPercentage += Number(i.percentage);
      });
      return totalPercentage === 100;
    } else
      return true;
  }

  showGreenTickForMarketingGoals() {
    if ((this.selectedClient.marketingGoals1 !== "" && this.selectedClient.marketingGoals1 !== null) ||
      (this.selectedClient.marketingGoals2 !== "" && this.selectedClient.marketingGoals2 !== null) ||
      (this.selectedClient.marketingGoals3 !== "" && this.selectedClient.marketingGoals3 !== null)) {
      return true;
    }
    return false;
  }

  showGreenTickForMarketingChallenges() {
    if ((this.selectedClient.keyMarketingChallenges1 !== "" && this.selectedClient.keyMarketingChallenges1 !== null) ||
      (this.selectedClient.keyMarketingChallenges2 !== "" && this.selectedClient.keyMarketingChallenges2 !== null) ||
      (this.selectedClient.keyMarketingChallenges3 !== "" && this.selectedClient.keyMarketingChallenges3 !== null)) {
      return true;
    }
    return false;
  }

  checkSplitChannelChecked() {
    let checkedCount = 0;
    for (let i = 0; i < this.splitChannelPercentageCheckbox.length; i++) {
      if (this.splitChannelPercentageCheckbox[i].checked) {
        checkedCount++;
      }
    }
    return checkedCount;
  }

  checkPercentageRange() {
    let flag = true;
    for (let i = 0; i < this.splitChannelPercentageCheckbox.length; i++) {
      if (this.splitChannelPercentageCheckbox[i].checked) {
        if (!(Number(this.splitChannelPercentageCheckbox[i].percentage) > 0 && Number(this.splitChannelPercentageCheckbox[i].percentage) < 101)) {
          flag = false;
          break;
        }
      }
    }
    return flag;
  }

  clearBudgetSelection() {
    const ele = document.getElementsByName("InfrastructureDevelopmentBudget");
    for (let i = 0; i < ele.length; i++)
      (<HTMLInputElement>ele[i]).checked = false;
    this.selectedClient.infrastructureDevBudget = '';
  }

  OnClickContinue() {
    if (this.isAddClientModalUsed) {
      this.finalClientData['ci_id'] = this.clientModalResponse['ciId'];
    } else {
      this.finalClientData['ci_id'] = -1;
    }

    // console.log(this.checkSplitChannelChecked(), this.checkPercentageRange());
    // console.log(this.checkSplitChannelChecked() === 0 || !this.checkPercentageRange());
    // validation checks
    if (this.selectedClient.clientName === "" || this.selectedClient.clientName === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.businessGoal === "" || this.selectedClient.businessGoal === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.businessVertical === "" || this.selectedClient.businessVertical === null) {
      this.hideErrorMsg = false;
    } else if (this.checkSplitChannelChecked() > 0 && !this.checkPercentageRange()) {
      console.log("split channels show error");
      this.hidePercentageErrorMsg = false;
    }
      // else if(this.checkSplitChannelChecked() === 0) {
      //   this.hideErrorMsg = false;
      //   this.hideSplitChannelErrorMsg = false;
      //   console.log("null");
      // }
      // else if(this.selectedClient.keyMarketingChallenges1 === "" || this.selectedClient.keyMarketingChallenges1 === null) {
      //   this.hideErrorMsg = false;
      // } else if(this.selectedClient.keyMarketingChallenges2 === "" || this.selectedClient.keyMarketingChallenges2 === null) {
      //   this.hideErrorMsg = false;
      // } else if(this.selectedClient.keyMarketingChallenges3 === "" || this.selectedClient.keyMarketingChallenges3 === null) {
      //   this.hideErrorMsg = false;
    // }
    else if ((this.selectedClient.keyMarketingChallenges1 === "" || this.selectedClient.keyMarketingChallenges1 === null) &&
      (this.selectedClient.keyMarketingChallenges2 === "" || this.selectedClient.keyMarketingChallenges2 === null) &&
      (this.selectedClient.keyMarketingChallenges3 === "" || this.selectedClient.keyMarketingChallenges3 === null)) {
      this.hideErrorMsg = false;
    } else if ((this.selectedClient.marketingGoals1 === "" || this.selectedClient.marketingGoals1 === null) &&
      (this.selectedClient.marketingGoals2 === "" || this.selectedClient.marketingGoals2 === null) &&
      (this.selectedClient.marketingGoals3 === "" || this.selectedClient.marketingGoals3 === null)) {
      this.hideErrorMsg = false;
      // console.log("marketing goal");
    } else if (this.selectedClient.mediaAgency === "" || this.selectedClient.mediaAgency === null) {
      this.hideErrorMsg = false;
    } else if (this.selectedClient.mediaHoldingGroup === "" || this.selectedClient.mediaHoldingGroup === null) {
      this.hideErrorMsg = false;
    }
      // else if(this.selectedItemsStakeholder.length === 0) {
      //   this.hideErrorMsg = false;
    // }
    else if (this.stakeholders1.length === 0) {
      this.hideErrorMsg = false;
    } else {
      // this.hidePercentageErrorMsg = true;
      console.log(this.finalClientData);

      if (this.checkSplitChannelChecked() === 0 || (this.checkSplitChannelChecked() > 0 && this.checkPercentageRange())) {
        this.hidePercentageErrorMsg = true;
        console.log("split channels hide error");
      }

      this.finalClientData.businessGoal = this.selectedClient.businessGoal;
      this.finalClientData['businessSize'] = this.selectedClient.businessSize;
      this.finalClientData['businessType'] = this.selectedClient.businessType;
      this.finalClientData['businessVertical'] = this.selectedClient.businessVertical;
      this.finalClientData['clientName'] = this.selectedClient.clientName;
      this.finalClientData['companyName'] = "Company1";
      this.finalClientData['infraDevlopBudget'] = this.selectedClient.infrastructureDevBudget;
      this.finalClientData['keyMarketChallenge'] = [];
      if (this.selectedClient.keyMarketingChallenges1 !== "") {
        this.finalClientData['keyMarketChallenge'].push(this.selectedClient.keyMarketingChallenges1);
      }
      if (this.selectedClient.keyMarketingChallenges2 !== "") {
        this.finalClientData['keyMarketChallenge'].push(this.selectedClient.keyMarketingChallenges2);
      }
      if (this.selectedClient.keyMarketingChallenges3 !== "") {
        this.finalClientData['keyMarketChallenge'].push(this.selectedClient.keyMarketingChallenges3);
      }
      // this.finalClientData['keyMarketChallenge'].push(this.selectedClient.keyMarketingChallenges1);
      // this.finalClientData['keyMarketChallenge'].push(this.selectedClient.keyMarketingChallenges2);
      // this.finalClientData['keyMarketChallenge'].push(this.selectedClient.keyMarketingChallenges3);
      this.finalClientData['marketingBudget'] = this.selectedClient.marketingBudget;
      this.finalClientData['marketingGoals'] = [];
      if (this.selectedClient.marketingGoals1 !== "") {
        this.finalClientData['marketingGoals'].push(this.selectedClient.marketingGoals1);
      }
      if (this.selectedClient.marketingGoals2 !== "") {
        this.finalClientData['marketingGoals'].push(this.selectedClient.marketingGoals2);
      }
      if (this.selectedClient.marketingGoals3 !== "") {
        this.finalClientData['marketingGoals'].push(this.selectedClient.marketingGoals3);
      }
      // this.finalClientData['marketingGoals'].push(this.selectedClient.marketingGoals1);
      // this.finalClientData['marketingGoals'].push(this.selectedClient.marketingGoals2);
      // this.finalClientData['marketingGoals'].push(this.selectedClient.marketingGoals3);
      this.finalClientData['mediaAgency'] = this.selectedClient.mediaAgency;
      this.finalClientData['mediaHoldingGroup'] = this.selectedClient.mediaHoldingGroup;
      this.finalClientData['ownerName'] = "Tom Richard";
      this.finalClientData['seName'] = "Saurabh";

      this.finalStakeholders = [];
      // for(let i=0;i<this.selectedItemsStakeholder.length; i++) {
      //   this.finalStakeholders.push(this.selectedItemsStakeholder[i].itemName);
      // }

      this.finalStakeholders = this.stakeholders1;

      this.finalClientData['stakeholderList'] = this.finalStakeholders;
      this.interactionService.setStakeholders(this.finalStakeholders);
      this.finalClientData['splitChannelMap'] = {};
      for (let j = 0; j < this.splitChannelPercentageCheckbox.length; j++) {
        if (this.splitChannelPercentageCheckbox[j].checked) {
          let obj = {
            [this.splitChannelPercentageCheckbox[j].channel]: this.splitChannelPercentageCheckbox[j].percentage
          }
          this.finalClientData['splitChannelMap'][this.splitChannelPercentageCheckbox[j].channel] = this.splitChannelPercentageCheckbox[j].percentage;
        }
      }
      console.log(this.finalClientData);
      const url = environment.addReview;
      // API to submit data
      this.http.post(url, this.finalClientData)
        .subscribe(res => {
          this.dataPersistanceService.setPersistance(true);
          this.dataPersistanceService.setData(this.finalClientData);
          this.dataPersistanceService.setResultGET(this.result);
          this.isLoading = true;
          this.interactionService.setReviewId(res['reviewMaster']['rm_id']);
          this.isLoading = false;
          this.router.navigate(['/prequestionnairepage']);
        }, err => {
          console.log("client error", err);
          this.addClientError = err.message;
          // alert("Please fill all the fields before confirm");
          $('#errorHTTPMsgModal').modal('show');
        });
    }
  }
}
