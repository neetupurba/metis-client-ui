export const environment = {
  production: true,
  homePage : "https://metis.miqdigital.com/metis-web-service/homeInfo",
  createReview : "https://metis.miqdigital.com/metis-web-service/createReview",
  addClient : "https://metis.miqdigital.com/metis-web-service/addClient",
  addStakeholder : "https://metis.miqdigital.com/metis-web-service/addStakeholder",
  addMediaAgency : "https://metis.miqdigital.com/metis-web-service/addMediaAgency",
  addReview : "https://metis.miqdigital.com/metis-web-service/addReview",
  review : "https://metis.miqdigital.com/metis-web-service/review",
  questionall : "https://metis.miqdigital.com/metis-web-service/questionall",
  stakeholderQuestionList : "https://metis.miqdigital.com/metis-web-service/stakeholderQuestionList",
  delete : "https://metis.miqdigital.com/metis-web-service/deleteReview",
  sendEmail : "https://metis.miqdigital.com/metis-web-service/sendEmail"
};
